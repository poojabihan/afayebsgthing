//
//  BoxSIViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/8/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class BoxSIViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var youtubeView: YTPlayerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "Box"
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        youtubeView.load(withVideoId: "r8ZCBZLFBR0")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: ScreenUtils.CurrentDeviceWidth, height: contactView.frame.origin.y + contactView.frame.size.height + 10)
    }
    

    @IBAction func contactBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Contact")
    }
    
    @IBAction func supoortBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Support")
    }
    
    @IBAction func SCRMBoxIntBuyNow_BtnAction(_ sender: Any) {
        
        pushToQuickBooksDetails(type: " SCRMBoxIntegrationBuyNow")
    }
    
    @IBAction func phoneNumberBtnAction(_ sender: Any) {
        guard let number = URL(string: "tel://" + "8182804820") else { return }
        UIApplication.shared.open(number)
    }
    
   
    
    
    
    func pushToQuickBooksDetails(type : String) {
        let boxSIWebViewVC = self.storyboard?.instantiateViewController(withIdentifier: "BoxSIWebViewVC") as! BoxSIWebViewVC
        boxSIWebViewVC.typeOfStr = type
        self.navigationController?.pushViewController(boxSIWebViewVC, animated: true)
    }

}
