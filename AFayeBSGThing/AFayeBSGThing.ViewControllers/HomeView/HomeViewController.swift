//
//  HomeViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 4/28/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import SDWebImage

class HomeViewController: UIViewController {

    //MARK:- Variable declaration
    var homeSectionOneModelArray  : HomeSectionOneModel = HomeSectionOneModel()
    var homeSectionTwoModelArray  : HomeSectionTwoModel = HomeSectionTwoModel()
    var homeSectionThreeModelArray  : HomeSectionThreeModel = HomeSectionThreeModel()

    //MARK:- IBOutlets
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var bannerImageView: UIImageView!
    
    @IBOutlet weak var footerView: UIView!

    @IBOutlet weak var homeSectionView_One: UIView!
    @IBOutlet weak var homeSectionImageView_One: UIImageView!
    @IBOutlet weak var homeSectionLbl_One: UILabel!
    @IBOutlet weak var homeSectionBtn_One: UIButton!
    
    
    @IBOutlet weak var homeSectionView_Two: UIView!
    @IBOutlet weak var homeSectionImageView_Two: UIImageView!
    @IBOutlet weak var homeSectionBtn_Two: UIButton!
    
    @IBOutlet weak var homeSectionView_Three: UIView!
    @IBOutlet weak var homeSectionImageView_Three: UIImageView!
    @IBOutlet weak var homeSectionBtn_Three: UIButton!
    @IBOutlet weak var homeSectionLbl_Three: UILabel!

    //FooterView
    @IBOutlet weak var copyRightsLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        setDesignStyles()
        
        DispatchQueue.global(qos: .userInitiated).async {
            // Download file or perform expensive task
            
            self.getHomeSectionOneAPI()
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            // Download file or perform expensive task
            
           self.getHomeSectionTwoAPI()
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            // Download file or perform expensive task
            
            self.getHomeSectionThreeAPI()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollview.contentSize = CGSize(width: ScreenUtils.CurrentDeviceWidth, height: footerView.frame.maxY)
    }
    func setDesignStyles() {
        homeSectionBtn_One.backgroundColor = .clear
       // homeSectionBtn_One.layer.cornerRadius = 3
        homeSectionBtn_One.layer.borderWidth = 1.5
        homeSectionBtn_One.layer.borderColor = UIColor.white.cgColor
        
        let overlay_One = UIView(frame: CGRect(x: 0, y: 0, width: homeSectionImageView_One.frame.size.width, height: homeSectionImageView_One.frame.size.height))
        overlay_One.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        homeSectionImageView_One.addSubview(overlay_One)

        homeSectionBtn_Two.backgroundColor = .clear
       // homeSectionBtn_Two.layer.cornerRadius = 3
        homeSectionBtn_Two.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        homeSectionBtn_Two.layer.borderWidth = 1.5
        homeSectionBtn_Two.layer.borderColor = UIColor.white.cgColor
        
        let overlay_Two = UIView(frame: CGRect(x: 0, y: 0, width: homeSectionImageView_Two.frame.size.width, height: homeSectionImageView_Two.frame.size.height))
        overlay_Two.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        homeSectionImageView_Two.addSubview(overlay_Two)
        
        homeSectionBtn_Three.backgroundColor = .clear
        // homeSectionBtn_Three.layer.cornerRadius = 3
//        homeSectionBtn_Three.backgroundColor = UIColor.white.withAlphaComponent(0.6)
//        homeSectionBtn_Three.layer.borderWidth = 1.5
//        homeSectionBtn_Three.layer.borderColor = UIColor.white.cgColor

        let overlay_Three = UIView(frame: CGRect(x: 0, y: 0, width: homeSectionImageView_Three.frame.size.width, height: homeSectionImageView_Three.frame.size.height))
        overlay_Three.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        homeSectionImageView_Three.addSubview(overlay_Three)

        //FooterView
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        copyRightsLbl.text = "Copyright FayeBSG, \(year)"

    }

    func getHomeSectionOneAPI()
    {
        //GenericMethods.showLoading()

        WebAPIHelper.getMethod(methodName: "/ApiHome.php?method=home_section1", success: {
            (response)->Void in
            
            print("home_section1 Response \(response!)")
            DispatchQueue.main.async{
                GenericMethods.hideLoading()
            }
            
            if !(response is NSNull)
            {
                self.homeSectionOneModelArray = Parser.parseHomeSectionOneModel(apiResponse: response!)
                
                if self.homeSectionOneModelArray.message == "Success"{
                    
                    DispatchQueue.main.async{
                        
                        self.homeSectionImageView_One.sd_setImage(with: URL(string: self.homeSectionOneModelArray.dataFeed[0].home_section_one_image as String), placeholderImage: UIImage(named: ""))
                        self.homeSectionLbl_One.text = self.homeSectionOneModelArray.dataFeed[0].home_section_one_text
                        self.homeSectionBtn_One.setTitle(self.homeSectionOneModelArray.dataFeed[0].home_section_one_button_text, for: .normal)
                    }
                }
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
        }, Failure: {
            (error)->Void in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        })
        
    }
    
    
    func getHomeSectionTwoAPI()
    {
        //GenericMethods.showLoading()
        
        WebAPIHelper.getMethod(methodName: "/ApiHome.php?method=home_section2", success: {
            (response)->Void in
            
            print("home_section2 Response \(response!)")
            DispatchQueue.main.async{
                GenericMethods.hideLoading()
            }
            
            if !(response is NSNull)
            {
                self.homeSectionTwoModelArray = Parser.parseHomeSectionTwoModel(apiResponse: response!)
                if self.homeSectionTwoModelArray.message == "Success"{
                    DispatchQueue.main.async{
                        self.homeSectionImageView_Two.sd_setImage(with: URL(string: self.homeSectionTwoModelArray.dataFeed[0].home_section_two_image as String), placeholderImage: UIImage(named: ""))
                        self.homeSectionBtn_Two.setTitle(self.homeSectionTwoModelArray.dataFeed[0].home_section_two_button_text, for: .normal)
                    }
                }
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
        }, Failure: {
            (error)->Void in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        })
        
    }
    
    func getHomeSectionThreeAPI()
    {
        //GenericMethods.showLoading()
        
        WebAPIHelper.getMethod(methodName: "/ApiHome.php?method=home_section3", success: {
            (response)->Void in
            
            print("home_section2 Response \(response!)")
            DispatchQueue.main.async{
                GenericMethods.hideLoading()
            }
            
            if !(response is NSNull)
            {
                self.homeSectionThreeModelArray = Parser.parseHomeSectionThreeModel(apiResponse: response!)
                if self.homeSectionThreeModelArray.message == "Success"{
                    DispatchQueue.main.async{
                        self.homeSectionImageView_Three.sd_setImage(with: URL(string: self.homeSectionThreeModelArray.dataFeed[0].home_section_three_image as String), placeholderImage: UIImage(named: ""))
//                        self.homeSectionBtn_Three.setTitle(self.homeSectionThreeModelArray.dataFeed[0].home_section_three_button_text, for: .normal)
                        self.homeSectionLbl_Three.text = self.homeSectionThreeModelArray.dataFeed[0].home_section_three_text

                    }
                }
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
        }, Failure: {
            (error)->Void in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        })
        
    }

    @IBAction func homeSection_OneBtnAction(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name("ChangeIndex"), object: ["index": 2])

    }
    
    @IBAction func homeSection_TwoBtnAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("ChangeIndex"), object: ["index": 1])
    }
    
    @IBAction func homeSection_ThreeBtnAction(_ sender: Any) {
        let blogDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        blogDetailVC.post_idStr = self.homeSectionThreeModelArray.dataFeed[0].home_section_three_button_link
        blogDetailVC.strUrlToShare = self.homeSectionThreeModelArray.dataFeed[0].home_section_three_button_text
        self.navigationController?.pushViewController(blogDetailVC, animated: true)
    }

    
    //MARK:- FooterView Methods
    @IBAction func slideShareBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://www.slideshare.net/FayeBusinessSystemsGroup")
    }
    
    @IBAction func twitterBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://twitter.com/FBSG")
    }
    
    @IBAction func facebookBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://www.facebook.com/FayeBusinessSystemsGroup?ref=ts")
    }
    
    @IBAction func googleBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://plus.google.com/103423219342581009269")
    }
    
    @IBAction func linkedinBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://www.linkedin.com/company/684045/")
    }
    
    @IBAction func youtubeBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://www.youtube.com/user/FayeBusinessSystem?feature=mhee")
    }
    
    @IBAction func rssBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://fayebsg.com/feed/")
    }
    
    @IBAction func btnContactusAction(_ sender: UIButton) {
        
        let ContactUsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        ContactUsVC.showNav = true
        self.navigationController?.pushViewController(ContactUsVC, animated: true)
        
    }
    func openURLString(urlStr : String) {
        let url = URL(string: urlStr)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    
}
