//
//  CustomNotificationWebViewController.swift
//  FayeBSG
//
//  Created by Apple on 21/06/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class CustomNotificationWebViewController: UIViewController, UIWebViewDelegate{

    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var lblTitle: UILabel!
    var spinner: UIActivityIndicatorView!
    var urlStr : String = ""
    var navigationTitleStr : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.text = navigationTitleStr
        setupSpinner()
        spinner.startAnimating()
        var url : URL!
        url = URL(string: urlStr)!
        webView.scalesPageToFit = true
        webView.contentMode = .scaleAspectFit
        webView.loadRequest(URLRequest(url: url))
        
    }
    
    
    func setupSpinner(){
        spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height:40))
        spinner.color = UIColor.darkGray
        self.spinner.center = CGPoint(x:UIScreen.main.bounds.size.width / 2, y:UIScreen.main.bounds.size.height / 2)
        self.view.addSubview(spinner)
        spinner.hidesWhenStopped = true
    }
    
    // MARK:- WebView Delegates
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        NSLog("Webview started Loading")
        DispatchQueue.main.async {
            self.spinner.startAnimating()
        }
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("\nWebview did finish load", terminator: "")
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("WebView Failed")
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
        }
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        print("Navigation Url \(request.url!)")
        return true
    }
    

    @IBAction func backButtonAction(_ sender: Any) {
        
        //self.dismiss(animated: false, completion: nil)
        self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)

    }
    
    


}

