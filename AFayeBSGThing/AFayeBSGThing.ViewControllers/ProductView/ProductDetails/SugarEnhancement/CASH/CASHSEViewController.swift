//
//  CASHSEViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/9/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class CASHSEViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var contactBtn: UIButton!
    @IBOutlet weak var youtubeView: YTPlayerView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "CASH"
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
        youtubeView.load(withVideoId: "qAycPBZ0t60")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: ScreenUtils.CurrentDeviceWidth, height: contactBtn.frame.origin.y + contactBtn.frame.size.height + 10)
    }
    
    @IBAction func contactUsTodayBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "ContactUsLearnCashSE")
    }
    
    func pushToQuickBooksDetails(type : String) {
        let sugarMobileSEWebViewVC = self.storyboard?.instantiateViewController(withIdentifier: "SugarMobileSEWebViewVC") as! SugarMobileSEWebViewVC
        sugarMobileSEWebViewVC.typeOfStr = type
        self.navigationController?.pushViewController(sugarMobileSEWebViewVC, animated: true)
    }

}
