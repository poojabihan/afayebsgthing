//
//  ServicesViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 4/28/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit


class ServicesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    //MARK:- Variable declaration
  var servicesModelArray  : ServicesModel = ServicesModel()
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "ServiceCollectionViewCell", bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: "ServiceCollectionViewCell")
//        getServicesAPI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getServicesAPI()

    }
    
    func getServicesAPI()
    {
        GenericMethods.showLoading()
        
        WebAPIHelper.getMethod(methodName: "/ApiServices.php", success: {
            (response)->Void in
            
            print("getServicesAPI Response \(response!)")
            
            
            if !(response is NSNull)
            {
                self.servicesModelArray = Parser.parseServicesModel(apiResponse: response!)
                if self.servicesModelArray.message == "Success"{
                DispatchQueue.main.async{
                        GenericMethods.hideLoading()
                        self.collectionView.reloadData()
                    }
                    
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
        }, Failure: {
            (error)->Void in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        })
        
    }
   
    //MARK:- CollectionView Delegates
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return servicesModelArray.dataFeed.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCollectionViewCell", for: indexPath) as! ServiceCollectionViewCell
        
        let imageView = UIImageView()
        imageView.image = UIImage(named: "placeholder_Image")
        imageView.frame = CGRect(x: 0, y: 0, width: 55, height: 55)
        imageView.layer.cornerRadius = imageView.frame.width / 2;
        imageView.layer.masksToBounds = true
        
        cell.serviceImageView.sd_setImage(with: URL(string: self.servicesModelArray.dataFeed[indexPath.row].image as String), placeholderImage: imageView.image)
        
        cell.serviceNameLbl.text = self.servicesModelArray.dataFeed[indexPath.row].title
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width:collectionView.frame.size.width, height:128.0)
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        return CGSize(width:collectionView.frame.size.width, height:120.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            
            let sectionHeaderOneCellnib = UINib(nibName: "ServiceHeaderCell", bundle: nil)
            collectionView.register(sectionHeaderOneCellnib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ServiceHeaderCell")
            
            let headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ServiceHeaderCell", for: indexPath) as? ServiceHeaderCell
            
            
            return headerCell!
            
        case UICollectionElementKindSectionFooter:
            
            let sectionFooterCellnib = UINib(nibName: "SectionFooterCell", bundle: nil)
            collectionView.register(sectionFooterCellnib, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "SectionFooterCell")
            
            let footerCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionFooterCell", for: indexPath) as? SectionFooterCell
            
             footerCell?.btnContactUs.addTarget(self, action: #selector(self.pressContactButton(_:)), for: .touchUpInside) //<- use `#selector(...)`
            
            return footerCell!
            
            
        default:
            
            fatalError("Unexpected element kind")
        }
        
       

        
    }
    
    //The target function
    @objc func pressContactButton(_ sender: UIButton){ //<- needs `@objc`
        print("\(sender)")
        
        let ContactUsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        ContactUsVC.showNav = true
        self.navigationController?.pushViewController(ContactUsVC, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let margin = SCLAlertView.SCLAppearance.Margin(buttonSpacing: 30,
                                                       bottom: 30,
                                                       horizontal: 30)
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "HelveticaNeue", size: 16)!,
            kTextFont: UIFont(name: "HelveticaNeue", size: 12)!,
            kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 12)!,
            shouldAutoDismiss: true,
            margin: margin
        )
        
        
        let alert = SCLAlertView(appearance: appearance)
        
        let cell:ServiceCollectionViewCell = collectionView.cellForItem(at: indexPath) as! ServiceCollectionViewCell
       
        let imageView = UIImageView()
        imageView.image = cell.serviceImageView.image
        imageView.frame = CGRect(x: 0, y: 0, width: 128, height: 128)
        imageView.image = imageView.image?.imageWithColor(color1: UIColor.white)
        

        let color = Colors.THEMECOLOR
        
        
        _ = alert.showCustom(self.servicesModelArray.dataFeed[indexPath.row].title, subTitle: self.servicesModelArray.dataFeed[indexPath.row].text, color: color, circleIconImage: imageView.image)
        
        

    }
    
   

}
