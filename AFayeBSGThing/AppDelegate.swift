//
//  AppDelegate.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 4/28/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//
//
//com.chetaru.AFayeBSGThing
//com.chetaru.AFayeBSG

import UIKit
import Firebase
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var isLoading               : Bool = false
    var localNotificationView: UIView!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if UserDefaults.standard.object(forKey: "Push") == nil || (UserDefaults.standard.object(forKey: "Push") as? String) == "Yes"{
            registerForPushNotifications()
        }

        FirebaseApp.configure()
        Messaging.messaging().delegate = self as? MessagingDelegate
        
        let token = Messaging.messaging().fcmToken
        
        print("FCM token: \(token ?? "")")
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
       
        return true
    }
   
    
    
    
    @objc func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            sendAPNSTokenToServer(token: refreshedToken)
        }
    }
    
    func sendAPNSTokenToServer(token : String) {
        var pushDict: [String: Any] = [:]
        
        pushDict["device_id"] = DeviceDetails.UniqueId
        pushDict["device_token"] = token
        
        GenericMethods.showLoading()
        
        WebAPIHelper.postMethod(params: pushDict , methodName: "/ApiHome.php?method=saveToken", success: {
            (response)->Void in
            
            print("ApiHome.php?method=saveToken Response \(response!)")
            
            if !(response is NSNull)
            {
                DispatchQueue.main.async{
                    GenericMethods.hideLoading()
                }
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.hideLoading()
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                }
            }
        }, Failure: {
            (error)->Void in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
                GenericMethods.showAlert(alertMessage: ErrorMessages.SigninIncorrectDetailsAlert)
            }
        })
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        
    }
    
    
   func registerForPushNotifications() {
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else {
                self.showPermissionAlert()
                return
            }
            
            self.getNotificationSettings()
        }
        
        
    }
    
    
    @available(iOS 10.0, *)
    func getNotificationSettings() {
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            
            guard settings.authorizationStatus == .authorized else { return }
            
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    
    func showPermissionAlert() {
        let alert = UIAlertController(title: "Error", message: "Please enable access to Notifications in the Settings app.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
            self.gotoAppSettings()
            alert.dismiss(animated: false, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
    
    private func gotoAppSettings() {
        
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.openURL(settingsUrl)
        }
    }
    
    
    
    
    //MARK: - PushNotification methods iOS 10
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        
        if notificationSettings.types != .none {
            application.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("DeviceToken..\(token)")
        
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: "DeviceToken")
        
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("Failed to register:", error)
    }
    
    

    
    //Called when a notification is delivered to a foreground app.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("User Info = ",notification.request.content.userInfo)
        
        
        
        
        completionHandler([.alert, .badge, .sound])
    }
    
    //Called to let your app know which action was selected by the user for a given notification.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("User Info = ",response.notification.request.content.userInfo)
        
        
        
        let userInfo = response.notification.request.content.userInfo as NSDictionary
        print("Indict form \(userInfo)")
        print("tagvalue is = ",userInfo["tag"]!)
        
        var tagValueStr : String = ""
        
        tagValueStr = userInfo["tag"] as! String
        
        if tagValueStr == "home" {
            DispatchQueue.main.async{
                NotificationCenter.default.post(name: Notification.Name("ChangeIndex"), object: ["index": 0])
            }
            
        }
        else if tagValueStr == "product" {
            DispatchQueue.main.async{
                NotificationCenter.default.post(name: Notification.Name("ChangeIndex"), object: ["index": 2])
            }
            
        }
        else if tagValueStr == "service" {
            DispatchQueue.main.async{
                NotificationCenter.default.post(name: Notification.Name("ChangeIndex"), object: ["index": 3])
            }
        }
        else if tagValueStr == "blog" {
            DispatchQueue.main.async{
                NotificationCenter.default.post(name: Notification.Name("ChangeIndex"), object: ["index": 1])
            }
        }
        else if tagValueStr == "custom"{
            
            DispatchQueue.main.async{
    
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//
//        let centerViewController = mainStoryboard.instantiateViewController(withIdentifier: "CustomNotificationWebViewController") as! CustomNotificationWebViewController
//
//
//                if let url: String = userInfo["link"] as? String {
//
//                    print("\(url) Get URL")
//                     centerViewController.urlStr = url
//                    centerViewController.navigationTitleStr = userInfo["title"] as! String
//
//                }
//                self.window!.rootViewController = centerViewController
//
//                self.window!.makeKeyAndVisible()
                
               
                
  if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomNotificationWebViewController") as? CustomNotificationWebViewController {
    
            if let window = self.window, let rootViewController = window.rootViewController {
                
                
                NotificationCenter.default.post(name: Notification.Name("CustomNotificationReceived"), object:controller);
                
                var currentController = rootViewController
                if let url: String = userInfo["link"] as? String {
                    
                  print("\(url) Get URL")
                  controller.urlStr = url
                  controller.navigationTitleStr = userInfo["title"] as! String
                    
                                    }
                while let presentedController = currentController.presentedViewController {
                            currentController = presentedController
                    }
                
                currentController.present(controller, animated: false, completion: nil)
                    }
                }
             
            }
        }
        
        
        
        completionHandler()
        
    }
    
}

