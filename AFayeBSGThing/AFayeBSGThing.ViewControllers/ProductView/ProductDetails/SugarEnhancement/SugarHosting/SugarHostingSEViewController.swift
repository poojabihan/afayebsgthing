//
//  SugarHostingSEViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/9/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class SugarHostingSEViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var letusHostYouBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Sugar Hosting"
        self.automaticallyAdjustsScrollViewInsets = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: ScreenUtils.CurrentDeviceWidth, height: letusHostYouBtn.frame.origin.y + letusHostYouBtn.frame.size.height + 10)
    }
    
    
    @IBAction func sugarHostingGuideBtnAction(_ sender: Any) {
        if let url = URL(string: "https://campaign.fayebsg.com/acton/attachment/9763/f-0357/1/-/-/-/-/FayeBSG%20Sugar%20Hosting%20Guide.pdf") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction func hostingPriceInfo(_ sender: Any) {
        if let url = URL(string: "https://campaign.fayebsg.com/acton/attachment/9763/f-0307/1/-/-/-/-/Hosting%20Pricing.pdf") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
     @IBAction func letUsHostYouBtnAction(_ sender: Any) {
        
        pushToQuickBooksDetails(type: "LetUsHostYou")
     }
    
    func pushToQuickBooksDetails(type : String) {
        let sugarMobileSEWebViewVC = self.storyboard?.instantiateViewController(withIdentifier: "SugarMobileSEWebViewVC") as! SugarMobileSEWebViewVC
        sugarMobileSEWebViewVC.typeOfStr = type
        self.navigationController?.pushViewController(sugarMobileSEWebViewVC, animated: true)
    }
}
