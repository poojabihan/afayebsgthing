//
//  ServiceHelper.swift
//  TaxApp
//
//  Created by Sanjeev T on 29/12/16.
//  Copyright © 2016 Swift trials. All rights reserved.
//

import UIKit

class WebAPIHelper: NSObject {
    
    class func getMethod(methodName: String,  success: @escaping (AnyObject?)->Void, Failure: @escaping (NSError)->Void)
    {
        if Connectivity.isConnectedToNetwork() == false
        {
            GenericMethods.hideLoading()
            
            GenericMethods.showAlertWithTitle(alertTitleStr: AlertText.NoInternetConnection, alertMessage: AlertText.InternetConnection)
        }
        else {
            let session = URLSession.shared

            var urlPath : URL!
            urlPath = URL(string: "\(Config.BaseUrl)" + "\(methodName)")

            var request = URLRequest(url: urlPath! as URL)

            print( "JSON request is \(urlPath!)")
            request.httpMethod = "GET"

            request = generateHeaders(methodType: "GET", methodName: methodName, request: request)

            let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
                if(error == nil)
                {
                    do
                    {
                        if let httpResponse = response as? HTTPURLResponse
                        {
                            let statusCode = httpResponse.statusCode
                            if (statusCode == 426)
                            {
                               //force update
                                GenericMethods.hideLoading()
                                GenericMethods.showVersionUpdateAlert()

                            }
                            else if (statusCode == 503)
                            {
                                //App under maintenance
                                GenericMethods.hideLoading()
                                GenericMethods.showAlertWithTitle(alertTitleStr: AlertText.NoInternetConnection, alertMessage: AlertText.InternetConnection)
                            }
                            else if (statusCode == 401)
                            {
                                //unauthorize
                                GenericMethods.hideLoading()
                                GenericMethods.showAlert(alertMessage: NSLocalizedString("You are not authorized to access", comment: ""))
                            }
                            else if (statusCode == 200)
                            {
                                let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)
                                
                                success(JSON as AnyObject?)     // Closure being called as a function
                            }
                            else
                            {
                                let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)
                                print(JSON)
                                GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                            }
                        }
                        else
                        {
                       
                            let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)

                            success(JSON as AnyObject?)
                        }// Closure being called as a function
                    }
                    catch let JSONError as NSError
                    {
                        Failure(JSONError as NSError)
                        print("JSON Error is \(JSONError)")
                        GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                    }
                }
                else
                {
                    Failure(error! as NSError)
                    print("Error is \(error!)")
                    GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))

                }
            })
            task.resume()
        }
    }
    
    
    class func getMethodSocial(methodName: String,  success: @escaping (AnyObject?)->Void, Failure: @escaping (NSError)->Void)
    {
        if Connectivity.isConnectedToNetwork() == false
        {
            GenericMethods.hideLoading()
           GenericMethods.showAlertWithTitle(alertTitleStr: AlertText.NoInternetConnection, alertMessage: AlertText.InternetConnection)
        }
        else {
            let session = URLSession.shared
            
            var urlPath : URL!
            urlPath = URL(string: methodName)
            
            var request = URLRequest(url: urlPath! as URL)
            
            print( "JSON request is \(urlPath!)")
            request.httpMethod = "GET"
            
            request = generateHeaders(methodType: "GET", methodName: methodName, request: request)
            
            let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
                if(error == nil)
                {
                    do
                    {
                        if let httpResponse = response as? HTTPURLResponse
                        {
                            let statusCode = httpResponse.statusCode
                            if (statusCode == 426)
                            {
                                //force update
                                GenericMethods.hideLoading()
                                GenericMethods.showVersionUpdateAlert()
                                
                            }
                            else if (statusCode == 503)
                            {
                                //App under maintenance
                                GenericMethods.hideLoading()
                                GenericMethods.showAlertWithTitle(alertTitleStr: AlertText.NoInternetConnection, alertMessage: AlertText.InternetConnection)
                            }
                            else if (statusCode == 401)
                            {
                                //unauthorize
                                GenericMethods.hideLoading()
                                GenericMethods.showAlert(alertMessage: NSLocalizedString("You are not authorized to access", comment: ""))
                            }
                            else if (statusCode == 200)
                            {
                                let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)
                                
                                success(JSON as AnyObject?)     // Closure being called as a function
                            }
                            else
                            {
                                let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)
                                print(JSON)
                                let data = JSON as! NSDictionary
                                if data.value(forKey: "errors") != nil {
                                    let arr = data.value(forKey: "errors") as! NSArray
                  
                                    let dict = arr[0] as! NSDictionary
                                    if dict.value(forKey: "message") != nil{
                                        GenericMethods.showAlert(alertMessage: dict.value(forKey: "message") as! String)
                                    }
                                    else{
                                        GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                                    }
                                }
                                else if data.value(forKey: "error") != nil{
                                    let dict = data.value(forKey: "error") as! NSDictionary
                                    if dict.value(forKey: "message") != nil{
                                        GenericMethods.showAlert(alertMessage: dict.value(forKey: "message") as! String)
                                    }
                                    else{
                                        GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                                    }
                                }
                                else{
                                    GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                                }
                                
                                
                            }
                        }
                        else
                        {
                            
                            let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)
                            
                            success(JSON as AnyObject?)
                        }// Closure being called as a function
                    }
                    catch let JSONError as NSError
                    {
                        Failure(JSONError as NSError)
                        print("JSON Error is \(JSONError)")
                        GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                    }
                }
                else
                {
                    Failure(error! as NSError)
                    print("Error is \(error!)")
                    GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                    
                }
            })
            task.resume()
        }
    }
    
    class func postMethodSocial(params : [String: Any], methodName: String, success: @escaping (AnyObject?)->Void, Failure:@escaping (NSError)->Void)
    {
        do {
            
            if Connectivity.isConnectedToNetwork() == false
            {
                GenericMethods.hideLoading()
                GenericMethods.showAlertWithTitle(alertTitleStr: AlertText.NoInternetConnection, alertMessage: AlertText.InternetConnection)
            } else {
                
                let session = URLSession.shared
                let urlPath = URL(string: methodName)
                var request = URLRequest(url: urlPath! as URL)
                
                print( "JSON request is \(urlPath!) params \(params)")
                request.httpMethod = "POST"
                request.httpBody = try JSONSerialization.data(withJSONObject: params as Dictionary, options:.prettyPrinted)
                request = generateHeaders(methodType: "POST", methodName: methodName, request: request)
                
                
                let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
                    if(error == nil)
                    {
                        do
                        {
                            if let httpResponse = response as? HTTPURLResponse
                            {
                                let statusCode = httpResponse.statusCode
                                if (statusCode == 426)
                                {
                                    //force update
                                    GenericMethods.hideLoading()
                                    GenericMethods.showVersionUpdateAlert()
                                    
                                }
                                else if (statusCode == 503)
                                {
                                    //App under maintenance
                                    GenericMethods.hideLoading()
                                   GenericMethods.showAlertWithTitle(alertTitleStr: AlertText.NoInternetConnection, alertMessage: AlertText.InternetConnection)
                                }
                                else if (statusCode == 401)
                                {
                                    //unauthorize
                                    GenericMethods.hideLoading()
                                    GenericMethods.showAlert(alertMessage:NSLocalizedString("You are not authorized to access", comment: "") )
                                }
                                else if (statusCode == 200)
                                {
                                    let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)
                                    
                                    success(JSON as AnyObject?)     // Closure being called as a function
                                }
                                else
                                {
                                    let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)
                                    print(JSON)
                                    let data = JSON as AnyObject
                                    let dict = data.value(forKey: "error") as! NSDictionary
                                    if dict.value(forKey: "error_user_msg") != nil{
                                        GenericMethods.showAlert(alertMessage: dict.value(forKey: "error_user_msg") as! String)
                                    }
                                    else if dict.value(forKey: "message") != nil{
                                        GenericMethods.showAlert(alertMessage: dict.value(forKey: "message") as! String)
                                    }
                                    else {
                                         GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                                    }
                                    
                                }
                            }
                            else
                            {
                                let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as![String:AnyObject]
                                
                                success(JSON as AnyObject)     // Closure being called as a function
                            }
                        }
                        catch let JSONError as NSError
                        {
                            Failure(JSONError as NSError)
                            
                            print("JSON Error \(JSONError)")
                            GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                        }
                    }
                    else
                    {
                        Failure(error! as NSError)
                        GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                    }
                })
                task.resume()
            }
        }
        catch {
            print("Error in catch \(error.localizedDescription)")
        }
        
    }
    class func getMethodWithEncodedURL(methodName: String, encodedMethodName : String,  success: @escaping (AnyObject?)->Void, Failure: @escaping (NSError)->Void)
    {
        if Connectivity.isConnectedToNetwork() == false
        {
            GenericMethods.hideLoading()
           GenericMethods.showAlertWithTitle(alertTitleStr: AlertText.NoInternetConnection, alertMessage: AlertText.InternetConnection)
        }
        else {
            let session = URLSession.shared
            
            var urlPath : URL!
            urlPath = URL(string: "\(Config.BaseUrl)" + "\(encodedMethodName)")
            
            var request = URLRequest(url: urlPath! as URL)
            
            print( "JSON request is \(urlPath!)")
            request.httpMethod = "GET"
            
            request = generateHeaders(methodType: "GET", methodName: methodName, request: request)
            
            let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
                if(error == nil)
                {
                    do
                    {
                        if let httpResponse = response as? HTTPURLResponse
                        {
                            let statusCode = httpResponse.statusCode
                            if (statusCode == 426)
                            {
                                //force update
                                GenericMethods.hideLoading()
                                GenericMethods.showVersionUpdateAlert()
                                
                            }
                            else if (statusCode == 503)
                            {
                                //App under maintenance
                                GenericMethods.hideLoading()
                               GenericMethods.showAlertWithTitle(alertTitleStr: AlertText.NoInternetConnection, alertMessage: AlertText.InternetConnection)
                            }
                            else if (statusCode == 401)
                            {
                                //unauthorize
                                GenericMethods.hideLoading()
                                GenericMethods.showAlert(alertMessage:NSLocalizedString("You are not authorized to access", comment: "") )
                            }
                            else if (statusCode == 200)
                            {
                                let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)
                                
                                success(JSON as AnyObject?)     // Closure being called as a function
                            }
                            else
                            {
                                let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)
                                print(JSON)
                                GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                            }
                        }
                        else
                        {
                            let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)
                            
                            success(JSON as AnyObject?)     // Closure being called as a function
                        }
                    }
                    catch let JSONError as NSError
                    {
                        Failure(JSONError as NSError)
                        print("JSON Error is \(JSONError)")
                        GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                    }
                }
                else
                {
                    Failure(error! as NSError)
                    print("Error is \(error!)")
                    GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                    
                }
            })
            task.resume()
        }
    }
    
    class func postMethod(params : [String: Any], methodName: String, success: @escaping (AnyObject?)->Void, Failure:@escaping (NSError)->Void)
    {
        do {
            
            if Connectivity.isConnectedToNetwork() == false
            {
                GenericMethods.hideLoading()
               GenericMethods.showAlertWithTitle(alertTitleStr: AlertText.NoInternetConnection, alertMessage: AlertText.InternetConnection)
            } else {

                let session = URLSession.shared
                let urlPath = URL(string: "\(Config.BaseUrl)" + "\(methodName)")
                var request = URLRequest(url: urlPath! as URL)

//               print( "JSON request is \(urlPath!) params \(params)")
                request.httpMethod = "POST"
                request.httpBody = try JSONSerialization.data(withJSONObject: params as Dictionary, options:.prettyPrinted)
                request = generateHeaders(methodType: "POST", methodName: methodName, request: request)


                let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
                    if(error == nil)
                    {
                        do
                        {
                            if let httpResponse = response as? HTTPURLResponse
                            {
                                let statusCode = httpResponse.statusCode
                                if (statusCode == 426)
                                {
                                    //force update
                                    GenericMethods.hideLoading()
                                    GenericMethods.showVersionUpdateAlert()

                                }
                                else if (statusCode == 503)
                                {
                                    //App under maintenance
                                    GenericMethods.hideLoading()
                                   GenericMethods.showAlertWithTitle(alertTitleStr: AlertText.NoInternetConnection, alertMessage: AlertText.InternetConnection)
                                }
                                else if (statusCode == 401)
                                {
                                    //unauthorize
                                    GenericMethods.hideLoading()
                                    GenericMethods.showAlert(alertMessage: NSLocalizedString("You are not authorized to access", comment: ""))
                                }
                                else if (statusCode == 200)
                                {
                                    let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)

                                    success(JSON as AnyObject?)     // Closure being called as a function
                                }
                                else
                                {
                                    let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)
                                    print(JSON)
                                    GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                                }
                            }
                            else
                            {
                                let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as![String:AnyObject]
                                
                                success(JSON as AnyObject)     // Closure being called as a function
                            }
                        }
                        catch let JSONError as NSError
                        {
                            Failure(JSONError as NSError)

                            print("JSON Error \(JSONError)")
                            GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                        }
                    }
                    else
                    {
                        Failure(error! as NSError)
                        GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                    }
                })
                task.resume()
            }
        }
        catch {
            print("Error in catch \(error.localizedDescription)")
        }

    }
    
    class func postMethodWithArray(params : [[String: Any]], methodName: String, success: @escaping (AnyObject?)->Void, Failure:@escaping (NSError)->Void)
    {
        do {
            
            if Connectivity.isConnectedToNetwork() == false
            {
                GenericMethods.hideLoading()
               GenericMethods.showAlertWithTitle(alertTitleStr: AlertText.NoInternetConnection, alertMessage: AlertText.InternetConnection)
            } else {
                
                let session = URLSession.shared
                let urlPath = URL(string: "\(Config.BaseUrl)" + "\(methodName)")
                var request = URLRequest(url: urlPath! as URL)
                
                print( "JSON request is \(urlPath!) params \(params)")
                request.httpMethod = "POST"
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options:.prettyPrinted)
                request = generateHeaders(methodType: "POST", methodName: methodName, request: request)
                
                
                let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
                    if(error == nil)
                    {
                        do
                        {
                            if let httpResponse = response as? HTTPURLResponse
                            {
                                let statusCode = httpResponse.statusCode
                                if (statusCode == 426)
                                {
                                    //force update
                                    GenericMethods.hideLoading()
                                    GenericMethods.showVersionUpdateAlert()
                                    
                                }
                                else if (statusCode == 503)
                                {
                                    //App under maintenance
                                    GenericMethods.hideLoading()
                                   GenericMethods.showAlertWithTitle(alertTitleStr: AlertText.NoInternetConnection, alertMessage: AlertText.InternetConnection)
                                }
                                else if (statusCode == 401)
                                {
                                    //unauthorize
                                    GenericMethods.hideLoading()
                                    GenericMethods.showAlert(alertMessage: NSLocalizedString("You are not authorized to access", comment: ""))
                                }
                                else if (statusCode == 200)
                                {
                                    let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)
                                    
                                    success(JSON as AnyObject?)     // Closure being called as a function
                                }
                                else
                                {
                                    let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)
                                    print(JSON)
                                    GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                                }
                            }
                            else
                            {
                                let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as![String:AnyObject]
                                
                                success(JSON as AnyObject)     // Closure being called as a function
                            }
                        }
                        catch let JSONError as NSError
                        {
                            Failure(JSONError as NSError)
                            
                            print("JSON Error \(JSONError)")
                            GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                        }
                    }
                    else
                    {
                        Failure(error! as NSError)
                        GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                    }
                })
                task.resume()
            }
        }
        catch {
            print("Error in catch \(error.localizedDescription)")
        }
        
    }


    class func postMethodWithSingleArray(params : [Any], methodName: String, success: @escaping (AnyObject?)->Void, Failure:@escaping (NSError)->Void)
    {
        do {

            if Connectivity.isConnectedToNetwork() == false
            {
                GenericMethods.hideLoading()
               GenericMethods.showAlertWithTitle(alertTitleStr: AlertText.NoInternetConnection, alertMessage: AlertText.InternetConnection)
            } else {

                let session = URLSession.shared
                let urlPath = URL(string: "\(Config.BaseUrl)" + "\(methodName)")
                var request = URLRequest(url: urlPath! as URL)

                print( "JSON request is \(urlPath!) params \(params)")
                request.httpMethod = "POST"
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options:.prettyPrinted)
                request = generateHeaders(methodType: "POST", methodName: methodName, request: request)


                let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
                    if(error == nil)
                    {
                        do
                        {
                            if let httpResponse = response as? HTTPURLResponse
                            {
                                let statusCode = httpResponse.statusCode
                                if (statusCode == 426)
                                {
                                    //force update
                                    GenericMethods.hideLoading()
                                    GenericMethods.showVersionUpdateAlert()

                                }
                                else if (statusCode == 503)
                                {
                                    //App under maintenance
                                    GenericMethods.hideLoading()
                                   GenericMethods.showAlertWithTitle(alertTitleStr: AlertText.NoInternetConnection, alertMessage: AlertText.InternetConnection)
                                }
                                else if (statusCode == 401)
                                {
                                    //unauthorize
                                    GenericMethods.hideLoading()
                                    GenericMethods.showAlert(alertMessage: NSLocalizedString("You are not authorized to access", comment: ""))
                                }
                                else if (statusCode == 200)
                                {
                                    let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)

                                    success(JSON as AnyObject?)     // Closure being called as a function
                                }
                                else
                                {
                                    let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments)
                                    print(JSON)
                                    GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                                }
                            }
                            else
                            {
                                let JSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as![String:AnyObject]

                                success(JSON as AnyObject)     // Closure being called as a function
                            }
                        }
                        catch let JSONError as NSError
                        {
                            Failure(JSONError as NSError)

                            print("JSON Error \(JSONError)")
                            GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                        }
                    }
                    else
                    {
                        Failure(error! as NSError)
                        GenericMethods.showAlert(alertMessage: NSLocalizedString("Some Error Occurred. Please try again later.", comment: ""))
                    }
                })
                task.resume()
            }
        }
        catch {
            print("Error in catch \(error.localizedDescription)")
        }

    }

    class func generateHeaders(methodType:String, methodName:String, request:URLRequest) -> URLRequest
    {
        
        var request = request
        

        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
       
        let uniqueId: String? = UIDevice.current.identifierForVendor?.uuidString
        request.addValue(uniqueId!, forHTTPHeaderField: "UniqueId")
        request.addValue("IOS", forHTTPHeaderField: "RequestFrom")
       request.addValue(Config.CurrentVersion, forHTTPHeaderField: "Version")

        print(request.allHTTPHeaderFields!)
        
        return request as URLRequest
    }
    
    class func navigationController() -> UINavigationController
    {
        let navController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController

        return navController!
    }

}
