//
//  ProfileViewController.swift
//  FayeBSG
//
//  Created by kdstudio on 20/09/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtFname: UITextField!
    @IBOutlet weak var txtLname: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var tblView: UITableView!

    var imagePicker = UIImagePickerController()
    var isImageChanged = false
    var textField: UITextField?


    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker.delegate = self

        // Do any additional setup after loading the view.
        btnSave.layer.cornerRadius = 20
        imgProfile.layer.cornerRadius = 35

        webServiceCallingGetProfile()
        
        NotificationCenter.default.addObserver(self, selector: #selector(PageViewController.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PageViewController.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        self.hideKeyboardWhenTappedAround()


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnSaveAction() {
        webServiceCallingUpdateProfile()
    }
    
    @IBAction func btncloseAction() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        let alert = UIAlertController(title: "Choose your profile picture from any of this options", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func convertImageToBase64(imageData: Data) -> String {
//        let imageData = UIImagePNGRepresentation(image)!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    func webServiceCallingUpdateProfile()
    {
        var strImg = ""
        if self.isImageChanged {
            
            var imgData = UIImageJPEGRepresentation(imgProfile.image!, 1.0)!
            if let imageData = imgProfile.image!.jpeg(.medium) {
                print(imageData.count)
                imgData = imageData
            }

            strImg = convertImageToBase64(imageData: imgData)
        }
        
        GenericMethods.showLoading()
        WebAPIHelper.postMethod(params: ["email" : UserDefaults.standard.object(forKey: "Email") ?? "", "first_name":txtFname.text ?? "", "last_name": txtLname.text ?? "", "profile_picture": strImg], methodName: "/ApiHome.php?method=updateProfile", success: { (response) in
            
            print("Response \(response!)")
            DispatchQueue.main.async{
                GenericMethods.hideLoading()
            }
            
            if !(response is NSNull) {
                DispatchQueue.main.async{
                let alert = UIAlertController(title: "Success", message: "Profile Updated Successfully", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    self.dismiss(animated: false, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
            
        }) { (error) in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        }
    }

    func webServiceCallingGetProfile()
    {
        
        GenericMethods.showLoading()
        WebAPIHelper.postMethod(params: ["id" : UserDefaults.standard.object(forKey: "id") ?? ""], methodName: "/ApiHome.php?method=getProfile", success: { (response) in
            
            print("Response \(response!)")
            DispatchQueue.main.async{
                GenericMethods.hideLoading()
            }
            
            if !(response is NSNull) {
                DispatchQueue.main.async{
                    if response!["status"] as? Bool == true{
                    let arrResult = response?.value(forKeyPath: "result") as? NSArray
                    self.txtFname.text = (arrResult![0] as AnyObject).value(forKeyPath: "first_name") as? String
                    self.txtLname.text = (arrResult![0] as AnyObject).value(forKeyPath: "last_name") as? String
                    if (arrResult![0] as AnyObject).value(forKeyPath: "profile_picture") as? String != ""{
                        let profileUrl = (arrResult![0] as AnyObject).value(forKeyPath: "profile_picture") as? String
                        
                        if profileUrl != nil {
                            self.imgProfile.sd_setImage(with: URL(string: imgUrl + profileUrl!), placeholderImage: #imageLiteral(resourceName: "user_default.png"))
                        }
                    }
                }
                }
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
            
        }) { (error) in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        }
    }
    
    //MARK: - Custom Functions
    
    /*Action Sheet Options Function for Uploading File*/
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                self.isImageChanged = true
                imgProfile.image = image
                
                self.dismiss(animated: false, completion: nil)
            }
                
            else{
                picker.popViewController(animated: true)
            }
            
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - UITextFiled keyboard hide unhide methods
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    
    //MARK: - UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }

}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
}
