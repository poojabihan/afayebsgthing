//
//  AppointmentCell.swift
//  FayeBSG
//
//  Created by Apple on 03/11/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class AppointmentCell: UITableViewCell {

    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
