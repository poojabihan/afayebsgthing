//
//  SugarMobileSEWebViewVC.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/9/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import WebKit

class SugarMobileSEWebViewVC: UIViewController, WKUIDelegate, WKNavigationDelegate {

    var webView: WKWebView!
    var spinner: UIActivityIndicatorView!
    var typeOfStr : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Details"
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: CGRect(x: 0 , y: 0 , width: self.view.frame.size.width  , height: self.view.frame.size.height), configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.sizeToFit()
        self.view.addSubview(webView)
        setupSpinner()
        spinner.startAnimating()
        var url : URL!
        
        if typeOfStr == "ViewExtendedDemo" {
            //Contact us
            url = URL(string: "https://www.youtube.com/embed/ngpty-fJzTQ")!
        }
        else if typeOfStr == "BuildMyAp" {
            //specSheetBtnAction
            url = URL(string: "https://campaign.fayebsg.com/acton/form/9763/0138:d-0001/0/-/-/-/-/index.htm")!
        }
        else if typeOfStr == "ContactUsToday" {
            //versionMatrixBtnAction
            url = URL(string: "https://campaign.fayebsg.com/acton/form/9763/0138:d-0001/0/-/-/-/-/index.htm")!
        }
        else if typeOfStr == "LetUsHostYou" {
            //versionMatrixBtnAction
            url = URL(string: "https://campaign.fayebsg.com/acton/form/9763/00f3:d-0001/0/-/-/-/-/index.htm")!
        }
        
        else if typeOfStr == "ContactUDashboardManager" {
            //versionMatrixBtnAction
            url = URL(string: "http://campaign.fayebsg.com/acton/form/9763/00b2:d-0001/0/-/-/-/-/index.htm?")!
        }
        else if typeOfStr == "ContactUsCAR" {
            //versionMatrixBtnAction
            url = URL(string: "https://campaign.fayebsg.com/acton/form/9763/010f:d-0001/0/-/-/-/-/index.htm?")!
        }
        else if typeOfStr == "ContactUsLearnCashSE" {
            //versionMatrixBtnAction
            url = URL(string: "https://campaign.fayebsg.com/acton/form/9763/00f1:d-0001/0/-/-/-/-/index.htm")!
        }
        
        
        
        webView.load(URLRequest(url: url))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    
    
    func setupSpinner(){
        spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height:40))
        spinner.color = UIColor.darkGray
        self.spinner.center = CGPoint(x:UIScreen.main.bounds.size.width / 2, y:UIScreen.main.bounds.size.height / 2)
        self.view.addSubview(spinner)
        spinner.hidesWhenStopped = true
    }
    
    // MARK:- WebView Delegates
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        NSLog("Webview started Loading")
        DispatchQueue.main.async {
            self.spinner.startAnimating()
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print("Navigation Url \(navigationAction.request.url!)")
        if (navigationAction.navigationType == .linkActivated){
            decisionHandler(.cancel)
        } else {
            decisionHandler(.allow)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("\nWebview did finish load", terminator: "")
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
        }
        
    }
    
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("WebView Failed")
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
        }
    }

}
