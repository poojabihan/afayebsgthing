//
//  Parser.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 4/28/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import Foundation
import ObjectMapper

class Parser: NSObject {

    
    class func parseHomeSectionOneModel(apiResponse:AnyObject) -> HomeSectionOneModel
    {
        let parserObj = Mapper<HomeSectionOneModel>().map(JSON: apiResponse as! [String : Any])
        
        return parserObj!
    }
    
    class func parseHomeSectionTwoModel(apiResponse:AnyObject) -> HomeSectionTwoModel
    {
        let parserObj = Mapper<HomeSectionTwoModel>().map(JSON: apiResponse as! [String : Any])
        
        return parserObj!
    }
    
    class func parseHomeSectionThreeModel(apiResponse:AnyObject) -> HomeSectionThreeModel
    {
        let parserObj = Mapper<HomeSectionThreeModel>().map(JSON: apiResponse as! [String : Any])
        
        return parserObj!
    }

    
    class func parseSugarIntegrationModel(apiResponse:AnyObject) -> SugarIntegrationModel
    {
        let parserObj = Mapper<SugarIntegrationModel>().map(JSON: apiResponse as! [String : Any])
        
        return parserObj!
    }
    
    class func parseServicesModel(apiResponse:AnyObject) -> ServicesModel
    {
        let parserObj = Mapper<ServicesModel>().map(JSON: apiResponse as! [String : Any])
        
        return parserObj!
    }
    
    class func parseBlogHeaderModel(apiResponse:AnyObject) -> BlogHeaderModel
    {
        let parserObj = Mapper<BlogHeaderModel>().map(JSON: apiResponse as! [String : Any])
        
        return parserObj!
    }
    class func parseBlogModel(apiResponse:AnyObject) -> BlogModel
    {
        let parserObj = Mapper<BlogModel>().map(JSON: apiResponse as! [String : Any])
        
        return parserObj!
    }
    
    class func parseBlogDetailModel(apiResponse:AnyObject) -> BlogDetailModel
    {
        let parserObj = Mapper<BlogDetailModel>().map(JSON: apiResponse as! [String : Any])
        
        return parserObj!
    }
    
}
