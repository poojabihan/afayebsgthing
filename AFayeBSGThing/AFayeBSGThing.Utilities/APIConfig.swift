//
//  APIConfig.swift
//  TruckLogicsLite
//
//  Created by Span Technology Services on 28/09/17.
//  Copyright © 2017 SPAN Technology Services. All rights reserved.
//

import Foundation
import UIKit

 let imgUrl = "https://fayebsg.com/webservice/profile_images/"

    /*
        APIConfig.swift has Config(URL for Sprint, Staging, Live) and WebApi(All API of App) maintained as Struct
    */


//  MARK:- Config Structure
struct Config
{
    static let appVersion: String = "/ios/1.4.1"
    static let CurrentVersion: String = "1.4.1"
    static let AppName: String = "MyUnitBuzz"
    static let AppstoreUrl: String = "https://itunes.apple.com/app/id1068478993"
    
    //Live
    static let BaseUrl = "https://fayebsg.com/webservice/"
    //Production
//    static let BaseUrl = "http://chetaru.gottadesigner.com/fayebsg_app/webservice"
    static let BaseImageUrl = "http://c.spanunit.com"
    static let publicKey = "Or2tf5TjnfLObg5qZ1VfLOd"
    static let privateKey = "ZujprDvpDDi4lvcitlgaRksJtpKxT7SOHiMxoB17i28"
    static let isLive: Bool = false


    static let isiPad: Bool = (UIDevice.current.userInterfaceIdiom == .pad ? true : false)
}


// MARK:- WEB API Structure
struct WebApi {

    // MARK:- API
    static let SIGNIN = "MyUnitBuzz/SignIn"
    static let SIGNUP = "TLLiteUser/SignUp"
    static let IS_EMAILADDRESS_ALREADY_EXIST = "TLLiteUser/IsEmailAddressAlreadyExist"
    static let FORGOT_PASSWORD = "TLLiteUser/ForgotPassword"
    static let VEHICLE_LIST_BY_USER_ID = "TLLiteUser/VehicleListByUserId"
    static let STATIC_DATA_LIST_BY_USERID = "TLLiteUser/StaticDataListByUserId"
    static let INITIAL_LOAD_FUELUP_LIST = "TLLiteUser/InitialLoadFuelUpList"
    static let INITIAL_LOAD_OTHEREXPENSETRANSACTION_LIST = "TLLiteUser/InitialLoadOtherExpenseTransactionList/?UserId="
    static let INITIAL_LOAD_INCOMETRANSACTION_LIST = "TLLiteUser/InitialLoadIncomeTransactionList?UserId="
    static let CHANGEPASSWORD = "User/ChangePassword"
    static let GETPASSWORDRESETDETAIL = "User/GetPasswordResetDetailsByUniqueIdandUserId"

    static let SYNC_OTHEREXPENSETRANSACTION_FROM_WEB_TO_DEVICE = "TLLiteUser/SyncOtherexpenseTransactionFromWebToDevice?MobileDeviceId=\(UserDefaults.standard.object(forKey: "MobileDeviceId")!)"

}

struct AlertText {
    static let NoInternetConnection = "No Internet Conection"
    static let InternetConnection = "Internet Connection appears to be offline, Please try again later"
    
}

