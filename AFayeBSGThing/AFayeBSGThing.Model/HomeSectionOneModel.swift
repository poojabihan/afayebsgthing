//
//  HomeSectionOneModel.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 4/28/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import ObjectMapper

class HomeSectionOneModel: Mappable {
  

    var dataFeed = [result]()
    var status : Bool = false
    var message : String = ""
    
    
    required init?(map: Map)
    {
        
    }
    init()
    {
        
        
    }
    func mapping(map: Map)
    {
        dataFeed <- map["result"]
        status <- map["status"]
        message <- map["message"]
        
        
    }
    
    
}

class result: Mappable
{
    var home_section_one_image :String = ""
    var home_section_one_text :String = ""
    var home_section_one_button_text :String    = ""
    var home_section_one_button_link :String = ""

    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    func mapping(map: Map)
    {
        home_section_one_image                   <- map["home_section_one_image"]
        home_section_one_text                    <- map["home_section_one_text"]
        home_section_one_button_text                <- map["home_section_one_button_text"]
        home_section_one_button_link                 <- map["home_section_one_button_link"]

    }
    
}
