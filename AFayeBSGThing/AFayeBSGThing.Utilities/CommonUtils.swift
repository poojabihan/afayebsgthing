//
//  WebAPI.swift
//  TruckLogicsLite
//
//  Created by STS-247 on 10/07/17.
//  Copyright © 2017 SPAN Technology Services. All rights reserved.
//

import Foundation
import UIKit

    /* 
        CommonUtils.swift has Structures that are used for Sync, ErrorMessages, Screen Utils, Field Length(TextField Character limits), Device Details
    */

struct SyncLogServiceNames {
    static let VEHICLE = "Vehicle"
    static let FUELUP = "FuelUp"
    static let EXPENSE = "Expense"
    static let USERACCOUNT = "UserAccount"
    static let CONTACT = "Contact"
    static let INCOME = "Income"
    static let MYPROFILE = "MyProfile"

}

struct ToastMessages
{
    //MARK:- MyProfile
    static let ProfileUpdatedSuccessfully = "Profile updated succesfully"
}
struct AlertMessages
{
    //MARK:- Forgot Pwd
    static let ForgotPwdTitle            = NSLocalizedString("Forgot Password", comment: "")
    static let ForgotPwdSucessAlert      = NSLocalizedString("The Temporary Password is sent to your registered email", comment: "")

    //MARK:- Registration
    static let RegistrationTitle        = NSLocalizedString("Registration Completed", comment: "")
    static let RegistrationSucessAlert  = NSLocalizedString("Your registration is successfully completed and an email notification will be received shortly!", comment: "")

    //MARK:- NewEvent
      static let newEventSucessAlert        = NSLocalizedString("Event added successfully", comment: "")
      static let newEventUpdateAlert        = NSLocalizedString("Event updated successfully", comment: "")
      static let DeleteEvent                = NSLocalizedString("Are you sure you want to delete this event?", comment: "")

    //MARK:- Chats - Group Creation
    static let GroupSucessAlert          = NSLocalizedString("Group created successfully", comment: "")
    static let GroupUpdateAlert          = NSLocalizedString("Group updated successfully", comment: "")
    static let BroadcastSucessAlert      = NSLocalizedString("Broadcast listed successfully", comment: "")
    static let BroadcastUpdateAlert      = NSLocalizedString("Broadcast updated successfully", comment: "")
    static let EmailDeleteMsg            = NSLocalizedString("Are you sure you want to delete this Message?", comment: "")
    static let EmailDeleteSucessAlert    = NSLocalizedString("Group message deleted successfully", comment: "")

    static let DeleteBroadcast           = NSLocalizedString("Are you sure you want to delete this broadcast?", comment: "")

    static let DeleteGroup               = NSLocalizedString("Are you sure you want to delete this Group?", comment: "")

    //MARK:- UnitCarrerPath
    static let blockAlertTitle                  = NSLocalizedString("Block", comment: "")
    static let blockAlertMsg                    = NSLocalizedString("Are you sure you want to block this contact's access?", comment: "")
    static let blockSuccessAlertMsg             = NSLocalizedString("This Contact's Access blocked successfully", comment: "")

    static let removeAlertTitle                  = NSLocalizedString("Remove", comment: "")
    static let removeAlertMsg                    = NSLocalizedString("Are you sure you want to remove this contact from the list?", comment: "")
    static let removeSuccessAlertMsg             = NSLocalizedString("Contact removed from the list", comment: "")

    

    //MARK:- Log out
    static let LogOutSucessAlert         = NSLocalizedString("Are you sure you want to log out?", comment: "")
    
    //MARK:- Invite/Access
    static let GrantAccessHeader        = NSLocalizedString("Grant Access", comment: "")
    static let GrantAccessSucces         = NSLocalizedString("Access granted successfully to this Contact", comment: "")
    
    static let ProvideAccessHeader        = NSLocalizedString("Provide Access", comment: "")
    static let ProvideAccessSucces         = NSLocalizedString("Access provided successfully to this Contact", comment: "")
    
    static let Access_Already_Granted   = NSLocalizedString("Contact's Access is already granted", comment: "")
    static let Access_Already_Removed   = NSLocalizedString("Contact's Access is already removed", comment: "")
    static let Access_Already_Blocked   = NSLocalizedString("Contact's Access is already blocked", comment: "")
    static let Access_Already_Denied   = NSLocalizedString("Contact's Access is already denied", comment: "")
    static let Access_Request_Exist   = NSLocalizedString("A Contact with same email address is already exist", comment: "")
    static let PendingRequest_Deny   = NSLocalizedString("Are you sure you want to deny this contact's access?", comment: "")
    static let PendingRequest_Deny_Yes   = NSLocalizedString("Yes,I want to deny", comment: "")
    static let PendingRequest_Deny_No   = NSLocalizedString("No,I don't want to deny", comment: "")
    
    static let ContactBlock   = NSLocalizedString("Are you sure you want to block this contact's access?", comment: "")
    static let ContactBlock_Yes   = NSLocalizedString("Yes,I want to block", comment: "")
    static let ContactBlock_No   = NSLocalizedString("No,I don't want to block", comment: "")
    
    static let ContactRemove   = NSLocalizedString("Are you sure you want to remove this contact from the list?", comment: "")
    static let ContactRemove_Yes   = NSLocalizedString("Yes,I want to remove", comment: "")
    static let ContactRemove_No   = NSLocalizedString("No,I don't want to remove", comment: "")
    
    
}

struct ErrorMessages {

    static let SomeErrorOccurred = NSLocalizedString("Some Error Occurred. Please try again later.", comment: "")
    static let invalidDataAlert = NSLocalizedString("Please Enter Valid data", comment: "")
    static let ProgressAlert    = NSLocalizedString("Page under progress", comment: "")

    //MARK:- Signin
    static let EmailAddressRequired = NSLocalizedString("Enter the Email Address", comment: "")
    static let EmailAddressInvalid = NSLocalizedString("Email Address is invalid", comment: "")
    static let unitNumberRequired = NSLocalizedString("Enter the Unit Number", comment: "")
    static let unitNumberInvalid = NSLocalizedString("Unit Number is invalid", comment: "")
    static let PasswordRequired = NSLocalizedString("Enter the Password", comment: "")
    static let PasswordRequiredMinimumSix = NSLocalizedString("Password is invalid. Should contain 6-12 characters", comment: "")
    static let SigninIncorrectDetailsAlert = NSLocalizedString("Invalid Email Address or Password", comment: "")
    static let NoAccessProvided = NSLocalizedString("Access has been declined, Please ask admin to Provide Access.", comment: "")


    //MARK:- Forgot Password
    static let invalidUserAlert = NSLocalizedString("This email address is already registered with us!", comment: "")
    static let incorrectDetailsAlert = NSLocalizedString("The details you have entered are incorrect. Check again and re-enter the valid details.", comment: "")

    //MARK:- Registration
    static let firstNameRequired = NSLocalizedString("Enter the First Name", comment: "")
    static let lastNameRequired = NSLocalizedString("Enter the Last Name", comment: "")
    static let PhoneNumberRequired = NSLocalizedString("Phone Number is Required", comment: "")
    static let PhoneNumberInvalid = NSLocalizedString("Phone Number is invalid", comment: "")
    static let MaryKayIdRequired = NSLocalizedString("Enter the MaryKay Id", comment: "")
    static let countryRequired = NSLocalizedString("Select Country", comment: "")
    static let languageRequired = NSLocalizedString("Select Language", comment: "")
    static let PositionRequired = NSLocalizedString("Select Position", comment: "")
    static let HtmlLoadAlert    = NSLocalizedString("Could not load the HTML string", comment: "")

    //MARK:- RequestAccess
    static let mkRankRequired = NSLocalizedString("Select mkRank", comment: "")
    static let DirectorNameRequired = NSLocalizedString("Enter the Director Name", comment: "")
    static let DirectorEmailRequired = NSLocalizedString("Enter the Director Email", comment: "")
    static let DirectorEmailInvalid = NSLocalizedString("Director Email is invalid", comment: "")

    //MARK:- AddCategory
    static let categoryNameRequired = NSLocalizedString("Enter the Category Name", comment: "")
    static let categoryExistAlert   = NSLocalizedString("Document category already exist!", comment: "")

    //MARk:- EventDetail
    static let RecurranceEditAlert = NSLocalizedString("Contact edited only on web", comment: "")

    //MARK:- NewEvent
    static let DateRequired = NSLocalizedString("Please Select Valid Date", comment: "")
    static let PurposeRequired = NSLocalizedString("Select a Purpose", comment: "")

    //MARK:- Chats_Group
    static let GroupNameRequired = NSLocalizedString("Enter the group name", comment: "")

    //MARK:- Chats_BroadCast
    static let ListNameRequired = NSLocalizedString("Enter the list name", comment: "")

    //MARK:- Email_Attachments
    static let AudioRecordFailed = NSLocalizedString("Failed to Record", comment: "")
    static let NoCameraAlert     = NSLocalizedString("This device has no Camera", comment: "")
    static let NoCallAlert       = NSLocalizedString("Your device could not make calls", comment: "")

    //MARK:- UnitCategoryDetail
    static let addressInvalid = NSLocalizedString("Address is Invalid", comment: "")

    //MARK:- Chats page
    static let UrlLinkInvalid = NSLocalizedString("Enter Valid URL", comment: "")

}

// MARK:- Screen Utils
struct ScreenUtils {

    static var CurrentDeviceHeight: CGFloat {
        struct Singleton {
            static let screenSize = UIScreen.main.bounds
            static let screenHeight = screenSize.height
        }
        return Singleton.screenHeight
    }

    static var CurrentDeviceWidth: CGFloat {
        struct Singleton {
            static let screenSize = UIScreen.main.bounds
            static let screenWidth = screenSize.width
        }
        return Singleton.screenWidth
    }
}

// MARK:- TextField Lengths
struct FieldLength {
    static let EMAILLENGTH = 100
    static let PASSWORDLENGTH = 12
    
    static let MARYKAYIDLENGTH = 50

    static let MESSAGELENGTH = 500

    static let GENERATEPWDLENGTH = 12

    static let MESSAGENOTIFICATIONLENGTH = 250
    
    static let AMOUNTLENGTH = 5
    static let AMOUNTWITHDECIMALLENGTH = 8

}

// MARK:- Device Details

struct DeviceDetails {
    static let UniqueId = UIDevice.current.identifierForVendor!.uuidString
    static let DeviceName = UIDevice.current.name
    static let Manufacturer = "Apple"
    static let Model = UIDevice.current.model
    static let DeviceOS = "ios"
    static let Version = UIDevice.current.systemVersion
    static let CurrentAppVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    static let DeviceTypeId = Config.isiPad ? "4" : "3"     //Device type Id(Eg: SmartPhone = 1, Tab = 2, iPhone = 3, iPad = 4)
}

struct AppConstants
{
    static let DefaultUserId = "00000000-0000-0000-0000-000000000000"
    static let DefaultBusinessId = "00000000-0000-0000-0000-000000000000"
    static let DefaultEmployeeId = "00000000-0000-0000-0000-000000000000"
}



