//
//  SectionFooterCell.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/1/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class SectionFooterCell: UICollectionViewCell {

    @IBOutlet weak var btnContactUs: UIButton!
    @IBOutlet weak var copyRightsLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        copyRightsLbl.text = "Copyright FayeBSG, \(year)"
    }

    
    
    @IBAction func slideShareBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://www.slideshare.net/FayeBusinessSystemsGroup")
    }
    
    @IBAction func twitterBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://twitter.com/FBSG")
    }
    
    @IBAction func facebookBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://www.facebook.com/FayeBusinessSystemsGroup?ref=ts")
    }
    
    @IBAction func googleBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://plus.google.com/103423219342581009269")
    }
    
    @IBAction func linkedinBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://www.linkedin.com/company/684045/")
    }
    
    @IBAction func youtubeBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://www.youtube.com/user/FayeBusinessSystem?feature=mhee")
    }
    
    @IBAction func rssBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://fayebsg.com/feed/")
    }
    
    
  
    
    func openURLString(urlStr : String) {
        let url = URL(string: urlStr)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
    
}
