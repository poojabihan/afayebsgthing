//
//  IntacctSIViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/8/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import youtube_ios_player_helper


class IntacctSIViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var youtubeView: YTPlayerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Intacct"
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        youtubeView.load(withVideoId: "eIhW5xu54AE")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: ScreenUtils.CurrentDeviceWidth, height: contactView.frame.origin.y + contactView.frame.size.height + 10)
    }
    
    @IBAction func contactBtnAction(_ sender: Any) {
         pushToQuickBooksDetails(type: "Contact")
    }
    
    @IBAction func SCRMIntacctInt_BtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "SCRMIntacctIntegration")
    }
    
    @IBAction func supportBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Support")
    }
    
    @IBAction func professionalEdition_BtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "ProfessionalEdition")
    }
    
    @IBAction func enterpriseEditionBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "EnterpriseEdition")
    }
    
    @IBAction func phoneNumberBtnAction(_ sender: Any) {
        guard let number = URL(string: "tel://" + "8182804820") else { return }
        UIApplication.shared.open(number)
    }
    
    func pushToQuickBooksDetails(type : String) {
        let intacctSIWebViewVC = self.storyboard?.instantiateViewController(withIdentifier: "IntacctSIWebViewVC") as! IntacctSIWebViewVC
        intacctSIWebViewVC.typeOfStr = type
        self.navigationController?.pushViewController(intacctSIWebViewVC, animated: true)
    }
}
