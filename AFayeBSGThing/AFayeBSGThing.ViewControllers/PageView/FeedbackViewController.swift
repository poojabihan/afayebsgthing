//
//  FeedbackViewController.swift
//  FayeBSG
//
//  Created by kdstudio on 20/09/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var txtFeedback: UITextView!
    @IBOutlet weak var btnSave: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        txtFeedback.layer.borderWidth = 1.0
//        txtFeedback.layer.borderColor = UIColor.lightGray.cgColor
        btnSave.layer.cornerRadius = 20
//        txtFeedback.layer.cornerRadius = 10
        txtFeedback.backgroundColor = UIColor.groupTableViewBackground
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSaveAction() {
        if txtFeedback.text.isEmpty {
            let alert = UIAlertController(title: "", message: "Please enter feedback.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.dismiss(animated: false, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            webServiceCallingForFeedback()
        }
    }
    
    @IBAction func btncloseAction() {
        self.dismiss(animated: false, completion: nil)
    }
    
    func webServiceCallingForFeedback()
    {
        
        GenericMethods.showLoading()
        WebAPIHelper.postMethod(params: ["suggestion_by" : UserDefaults.standard.object(forKey: "Email") ?? "", "suggestion": txtFeedback.text ], methodName: "/ApiHome.php?method=saveSuggestion", success: { (response) in
            
            print("Response \(response!)")
            DispatchQueue.main.async{
                GenericMethods.hideLoading()
            }
            
            if !(response is NSNull) {
                DispatchQueue.main.async{
                let alert = UIAlertController(title: "Success", message: "Feedback Submitted Successfully", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    self.dismiss(animated: false, completion: nil)
                }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
            
        }) { (error) in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
