//
//  VPMSEViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/9/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class VPMSEViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var youtubeView: YTPlayerView!
    @IBOutlet weak var contactUsBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Visual Property Manager"
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
        youtubeView.load(withVideoId: "K2BY1wo5hE0")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: ScreenUtils.CurrentDeviceWidth, height: contactUsBtn.frame.origin.y + contactUsBtn.frame.size.height + 10)
    }
    
    @IBAction func scheduleADemoBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "ScheduleADemo")
    }
    
    @IBAction func downloadVPMSheetBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "DownloadVPMSheet")
    }
    
    @IBAction func contactUsBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "ContactUs")
    }
    
    func pushToQuickBooksDetails(type : String) {
        let VPMSEWebViewVC = self.storyboard?.instantiateViewController(withIdentifier: "VPMSEWebViewVC") as! VPMSEWebViewVC
        VPMSEWebViewVC.typeOfStr = type
        self.navigationController?.pushViewController(VPMSEWebViewVC, animated: true)
    }
}
