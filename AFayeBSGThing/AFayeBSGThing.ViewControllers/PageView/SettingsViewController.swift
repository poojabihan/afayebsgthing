//
//  SettingsViewController.swift
//  FayeBSG
//
//  Created by kdstudio on 16/07/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var notificationSwitch: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Settings"
        

        let customBackButton = UIBarButtonItem(image: UIImage(named: "backArrow") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
        

        // Do any additional setup after loading the view.
        notificationSwitch.addTarget(self, action: #selector(stateChanged), for: UIControlEvents.valueChanged)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
        if UserDefaults.standard.object(forKey: "Push") == nil || (UserDefaults.standard.object(forKey: "Push") as? String) == "Yes"{
            notificationSwitch.isOn = true
        }
        else {
            notificationSwitch.isOn = false
        }
        //        self.title = strBlogHeading
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnAppointmentsAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AppointmentsList") as! AppointmentsList
    self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    @objc func backAction(sender: UIBarButtonItem) {
            self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func LogoutAction(sender: UIButton) {
        
        let alert = UIAlertController(title: nil, message: "Are you sure you want to Logout?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            UserDefaults.standard.removeObject(forKey: "LoggedIn")
            UserDefaults.standard.removeObject(forKey: "Email")
            self.navigationController?.popViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action) in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func stateChanged(switchState: UISwitch) {
        if switchState.isOn {
//            myTextField.text = "The Switch is On"
            UserDefaults.standard.set("Yes", forKey: "Push")
            DispatchQueue.main.async {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.registerForPushNotifications()
            }
            
        } else {
//            myTextField.text = "The Switch is Off"
            UserDefaults.standard.set("No", forKey: "Push")

            DispatchQueue.main.async {
                UIApplication.shared.unregisterForRemoteNotifications()
            }

        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
