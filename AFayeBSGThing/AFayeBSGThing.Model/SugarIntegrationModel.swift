//
//  SugarIntegrationModel.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 4/28/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import ObjectMapper

class SugarIntegrationModel: Mappable {

    var dataFeed = [sugerData]()
    var status : Bool = false
    var message : String = ""
    var service_name : String = ""
    
    
    required init?(map: Map)
    {
        
    }
    init()
    {
        
        
    }
    func mapping(map: Map)
    {
        
        status <- map["status"]
        message <- map["message"]
        dataFeed <- map["result"]
        service_name <- map["service_name"]
        
    }
    
}

class sugerData: Mappable
{
    var image :String = ""
    var title :String = ""
    var content :String    = ""
    var link :String    = ""
    
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    func mapping(map: Map)
    {
        image                   <- map["image"]
        title                    <- map["title"]
        content                <- map["content"]
        link                <- map["link"]
        
        
    }
    
}

