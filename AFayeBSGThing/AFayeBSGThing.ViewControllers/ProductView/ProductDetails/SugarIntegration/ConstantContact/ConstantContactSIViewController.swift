//
//  ConstantContactSIViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/8/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import youtube_ios_player_helper


class ConstantContactSIViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var youtubeView: YTPlayerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Constant Contact"
       
        self.automaticallyAdjustsScrollViewInsets = false
        
        youtubeView.load(withVideoId: "SqzzGnnU3FA")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: ScreenUtils.CurrentDeviceWidth, height: contactView.frame.origin.y + contactView.frame.size.height + 10)
    }
    

    @IBAction func contactusBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Contact")
    }
    
    @IBAction func SCRMConstantContactInt_BtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "SCRMConstantContactIntegration")
    }
    
    @IBAction func SCRMVersionMatrixBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "SCRMVersionMatrix")
    }
    
    @IBAction func supportBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Support")
    }
    
    @IBAction func specSheetBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "SpecSheet")
    }
    
    @IBAction func pressReleaseSCRMConstantInt_BtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "PressReleaseSCRMConstantIntegration")
    }
    
    @IBAction func professionalEditionBuyNowBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "ProfessionalEditionBuyNow")
    }
    
    @IBAction func phoneNumberBtnAction(_ sender: Any) {
        guard let number = URL(string: "tel://" + "8182804820") else { return }
        UIApplication.shared.open(number)
    }
    
    
    func pushToQuickBooksDetails(type : String) {
        let constantContactSIWebViewVC = self.storyboard?.instantiateViewController(withIdentifier: "ConstantContactSIWebViewVC") as! ConstantContactSIWebViewVC
        constantContactSIWebViewVC.typeOfStr = type
        self.navigationController?.pushViewController(constantContactSIWebViewVC, animated: true)
    }
}
