//
//  BlogsViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 4/28/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class BlogsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {

    //MARK:- Variable declaration
    @IBOutlet weak var blogHeaderCollectionView: UICollectionView!
    
    @IBOutlet weak var blogTableView: UITableView!
    @IBOutlet weak var bannerImageView: UIImageView!
    var headerBlogModelArray  : BlogHeaderModel = BlogHeaderModel()
    var blogModelArray  : BlogModel = BlogModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        setDesignStyles()
        let nib = UINib(nibName: "BlogHeaderCell", bundle: nil)
        blogHeaderCollectionView?.register(nib, forCellWithReuseIdentifier: "BlogHeaderCell")
        blogTableView.register(UINib(nibName:"BlogTableViewCell" , bundle: nil), forCellReuseIdentifier: "BlogTableViewCell")
//        DispatchQueue.global(qos: .userInitiated).async {
//            // Download file or perform expensive task
//
//            self.getHeaderBlogsAPI()
//        }
//
//        DispatchQueue.global(qos: .userInitiated).async {
//            // Download file or perform expensive task
//
//            self.getBlogsAPI()
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.global(qos: .userInitiated).async {
            // Download file or perform expensive task
            
            self.getHeaderBlogsAPI()
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            // Download file or perform expensive task
            
            self.getBlogsAPI()
        }

    }

    func setDesignStyles() {
        let overlay_Two = UIView(frame: CGRect(x: 0, y: 0, width: bannerImageView.frame.size.width, height: bannerImageView.frame.size.height))
        overlay_Two.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        bannerImageView.addSubview(overlay_Two)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getHeaderBlogsAPI()
    {
        GenericMethods.showLoading()
        
        WebAPIHelper.getMethod(methodName: "/ApiBlog.php?method=category", success: {
            (response)->Void in
            
            print("getHeaderBlogsAPI Response \(response!)")
            
            
            if !(response is NSNull)
            {
                
                
                
                self.headerBlogModelArray = Parser.parseBlogHeaderModel(apiResponse: response!)
                
                
                if self.headerBlogModelArray.message == "Success"{
                    
                    
                    
                    
                }
                DispatchQueue.main.async{
                    GenericMethods.hideLoading()
                    self.blogHeaderCollectionView.reloadData()
                    
                }
                
                
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
        }, Failure: {
            (error)->Void in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        })
        
    }
    
    func getBlogsAPI()
    {
        GenericMethods.showLoading()
        
        WebAPIHelper.getMethod(methodName: "/ApiBlog.php?method=blog", success: {
            (response)->Void in
            
            print("getBlogsAPI Response \(response!)")
            
            
            if !(response is NSNull)
            {
                
                self.blogModelArray = Parser.parseBlogModel(apiResponse: response!)
                if self.blogModelArray.message == "Success"{
                }
                DispatchQueue.main.async{
                    self.blogTableView.reloadData()
                    GenericMethods.hideLoading()
                }
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
        }, Failure: {
            (error)->Void in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        })
        
    }
    
    
    //MARK:- CollectionView Delegates
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
            return headerBlogModelArray.dataFeed.count
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BlogHeaderCell", for: indexPath) as! BlogHeaderCell
       
        cell.nameLbl.text = headerBlogModelArray.dataFeed[indexPath.row].name
        
        if indexPath.row == headerBlogModelArray.dataFeed.count - 1 {
            // Last cell is visible
            cell.lineLbl.isHidden = true
        }
        else{
            cell.lineLbl.isHidden = false
        }

        return cell
        
    }
       
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let fontAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13, weight: .medium)]
        let myText = headerBlogModelArray.dataFeed[indexPath.row].name
        let size = (myText as NSString).size(withAttributes: fontAttributes)
        
        
        let fixedsize =  CGSize(width: size.width+9, height: 30)
        //let size = headerBlogModelArray.dataFeed[indexPath.row].name.size(withAttributes: nil)
        return fixedsize
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let blogsCategoryVC = self.storyboard?.instantiateViewController(withIdentifier: "BlogsCategoryViewController") as! BlogsCategoryViewController
        blogsCategoryVC.category_idStr = headerBlogModelArray.dataFeed[indexPath.row].id
        blogsCategoryVC.categoryNameStr = headerBlogModelArray.dataFeed[indexPath.row].name
        self.navigationController?.pushViewController(blogsCategoryVC, animated: true)
    }
    
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let cellWidth : CGFloat = 82.0
        
        let numberOfCells = floor(self.blogHeaderCollectionView.frame.size.width / cellWidth)
        let edgeInsets = (self.blogHeaderCollectionView.frame.size.width - (numberOfCells * cellWidth)) / (numberOfCells + 1)
        
        return UIEdgeInsetsMake(0, edgeInsets + 10, 0, edgeInsets)
        
    }
   
    //MARK:- Tableview Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return self.blogModelArray.dataFeed.count
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:BlogTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BlogTableViewCell", for: indexPath) as! BlogTableViewCell
        cell.blogSubTitleNameLbl.numberOfLines = 0
        
        cell.blogImageView.sd_setImage(with: URL(string: self.blogModelArray.dataFeed[indexPath.row].image), placeholderImage: #imageLiteral(resourceName: "placeholder_Image"))
        
        cell.blogHeaderName.text = self.blogModelArray.dataFeed[indexPath.row].category
        cell.blogSubTitleNameLbl.text = self.blogModelArray.dataFeed[indexPath.row].title
      
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
            return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)

//        let blogDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "BlogDetailViewController") as! BlogDetailViewController
//        blogDetailVC.post_idStr = self.blogModelArray.dataFeed[indexPath.row].post_id
//
//        self.navigationController?.pushViewController(blogDetailVC, animated: true)
        
        let blogDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        blogDetailVC.post_idStr = self.blogModelArray.dataFeed[indexPath.row].post_id
        
        blogDetailVC.strUrlToShare = self.blogModelArray.dataFeed[indexPath.row].link
        blogDetailVC.strBlogHeading = self.blogModelArray.dataFeed[indexPath.row].title

        self.navigationController?.pushViewController(blogDetailVC, animated: true)
    }

}
