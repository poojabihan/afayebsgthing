//
//  BlogDetailViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/1/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import WebKit

class BlogDetailViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
  
    var spinner: UIActivityIndicatorView!
    var post_idStr : String = ""
    var blogDetailModelArray  : BlogDetailModel = BlogDetailModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Blog Details"
        self.automaticallyAdjustsScrollViewInsets = false
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "backArrow") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
        
        
        setupSpinner()
        webView.scalesPageToFit = true
        webView.contentMode = .scaleAspectFit
        
       getBlogDetailsAPI(postIDStr: post_idStr)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    

    func setupSpinner(){
        spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height:40))
        spinner.color = UIColor.darkGray
        self.spinner.center = CGPoint(x:UIScreen.main.bounds.size.width / 2, y:UIScreen.main.bounds.size.height / 2)
        self.view.addSubview(spinner)
        spinner.hidesWhenStopped = true
    }
    
    
    func getBlogDetailsAPI(postIDStr : String) {
        
            var blogDetailsDict: [String: Any] = [:]
            
            blogDetailsDict["post_id"] = postIDStr
            
            GenericMethods.showLoading()
            
            WebAPIHelper.postMethod(params: blogDetailsDict , methodName: "/ApiBlog.php?method=blog_detail", success: {
                (response)->Void in
                
                print("getBlogsCategoryWiseAPI Response \(response!)")
                
                if !(response is NSNull)
                {
                    
                    DispatchQueue.main.async {
                        GenericMethods.hideLoading()
                        
                    }
                    self.blogDetailModelArray = Parser.parseBlogDetailModel(apiResponse: response!)
                    
                    
                    if self.blogDetailModelArray.message == "Success"{
                        
                        
                        DispatchQueue.main.async {
                            print("HTML CODE is \(self.blogDetailModelArray.dataFeed[0].content)")
                            
                            
                            
                            self.spinner.startAnimating()
                            
                            self.webView.loadHTMLString(self.blogDetailModelArray.dataFeed[0].content, baseURL: nil)
                             
                        }
                        
                    }
                  
                    
                }
                else
                {
                    DispatchQueue.main.async {
                        GenericMethods.hideLoading()
                        GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    }
                }
            }, Failure: {
                (error)->Void in
                print("Error \(error)")
                DispatchQueue.main.async {
                    GenericMethods.hideLoading()
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SigninIncorrectDetailsAlert)
                }
            })
        
    }
    // MARK:- WebView Delegates
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        NSLog("Webview started Loading")
        DispatchQueue.main.async {
            self.spinner.startAnimating()
        }
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("\nWebview did finish load", terminator: "")
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("WebView Failed")
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
        }
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        print("Navigation Url \(request.url!)")
        return true
    }
   
    
    @objc func backAction(sender: UIBarButtonItem) {
        // custom actions here
        if(webView.canGoBack) {
            webView.goBack()
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
    }

}
