//
//  AppointmentsList.swift
//  FayeBSG
//
//  Created by Apple on 03/11/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class AppointmentsList: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnAdd: UIButton!

    var arrOfAppointmnets = [[String: Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        btnAdd.layer.cornerRadius = 23.0
    }
    
    

    @IBAction func btnAddAppointmentAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddAppointments") as! AddAppointments
        objVC.objParent = self
        objVC.modalPresentationStyle = .overCurrentContext
        self.present(objVC, animated: false, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        callWebServiceTogetAppointments()
        tblView.reloadData()
        if arrOfAppointmnets.count == 0 {
            
            lblNoData.isHidden = false
            lblNoData.text = "No Appointments Avaliable."
        }
        else{
            lblNoData.isHidden = true

        }

    }
    // MARK: - UITableView Data Souce and delegate
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfAppointmnets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : AppointmentCell = tableView.dequeueReusableCell(withIdentifier: "AppointmentCell") as! AppointmentCell
        
    cell.lblDate.text = arrOfAppointmnets[indexPath.row]["date"] as? String
        cell.lblTime.text = arrOfAppointmnets[indexPath.row]["time"] as? String
        cell.lblDescription.text = arrOfAppointmnets[indexPath.row]["description"] as? String
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    
    func callWebServiceTogetAppointments(){
        
        GenericMethods.showLoading()
        let param =
            [   "userId": UserDefaults.standard.value(forKey: "id") ?? ""] as [String : Any]
        
        print(param)
        
        WebAPIHelper.postMethod(params: param, methodName: "/ApiHome.php?method=getAppointment", success: { (response) in
            
            print("Response \(response!)")
            DispatchQueue.main.async{
                GenericMethods.hideLoading()
            }
            
            if !(response is NSNull) {
               
                DispatchQueue.main.async {
                    
                    //Code here
                    /*"result":[
                     {
                     "date": "23-11-2018",
                     "time": "6:00",
                     "description": "test",
                     "status": "pending"
                     }
                     ]*/
                    //print(response?.value(forKeyPath: "result") as! String)
                    self.arrOfAppointmnets = response?.value(forKeyPath: "result") as! Array
                    print(self.arrOfAppointmnets)
                    
                    if self.arrOfAppointmnets.count == 0 {
                        
                        self.lblNoData.isHidden = false
                        self.lblNoData.text = "No Appointments Avaliable."
                    }
                    else{
                        self.lblNoData.isHidden = true
                        
                    }
                    
                    self.tblView.reloadData()
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
            
        }) { (error) in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        }
        
    }
}
