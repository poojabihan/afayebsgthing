//
//  SugarCRMViewController.swift
//  AFayeBSGThing
//
//  Created by Apple on 20/06/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import WebKit

class SugarCRMViewController: UIViewController , WKNavigationDelegate
{
    //FooterView
    @IBOutlet weak var copyRightsLbl: UILabel!
    @IBOutlet weak var footerView: UIView!

//    @IBOutlet weak var webView: UIWebView!
//    @IBOutlet weak var webView: WKWebView!
    var webView: WKWebView!
    var colour: String?

    
    var spinner: UIActivityIndicatorView!

    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: "https://fayebsg.com/sugarcrm/")!
        webView.load(URLRequest(url: url))
        view = webView
        webView.isHidden = true
        webView.navigationDelegate = self

//        setupSpinner()
//        spinner.startAnimating()
        GenericMethods.showLoading()
        
        //FooterView
       /* let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        copyRightsLbl.text = "Copyright FayeBSG, \(year)"*/
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        
        
        let css = ".top_header {background: none #222;padding: 3px 0 0;display: none;}.aligncenter {    margin:10px 0 30px!important;    max-width: 100%!important;    height: auto!important;    }    .alignleft{    margin:10px 0 30px;    max-width: 100%;    height: auto;    }    img{    margin:10px 0 30px;    max-width: 100%;    height: auto;    }    iframe {    max-width: 100%;    height: auto !important;    margin-top:10px;    }    p{    margin: 0 0 20px;    text-align: justify;    }    .jive-image{    margin:10px 0 30px!important;    max-width: 100%!important;    height: auto!important;        }    .image-1{    margin:10px 0 30px !important;    max-width: 100% !important;    height: auto !important;}    table.j-table{width:100%!important;}    a{text-align:justify;}    .tablepress thead tr th, .tablepress tbody tr td {text-align: center;}    table{width:100%; max-width:100%;}    .header .logo {    margin: 0 auto;    float: none;}    #back-to-top,div#intercom-container,footer#footer, div#twitterbar,.header{    display: none;}    .banner {    margin-top: 0 !important;}    body div#back-to-top {    display: none !important;}    .bannercontainer.four.next.sugar_crm .content_rgt {width: 100%;}    #new-royalslider-35.royalSlider {    height: 175px !important;}    .bannercontainer.four.next.sugar_crm .content_rgt p { margin:0;}    .bannercontainer.four.next.sugar_crm .content_rgt img{margin:10px 0;}    input[type=\"button\"] {    border: 2px solid #035db8;    background-color: #035db8;    color: #ffffff;    padding: 5px 12px;}    .formField .formFieldMedium, .formField .formTextAreaWidthMedium {width: 100%;min-height:30px;margin:10px 0 0px;}    input[type=\"button\"] {border: 2px solid #035db8;background-color: #035db8;color: #ffffff; padding: 10px 20px;}    input,textarea{padding:5px 10px;}    body{margin-top:0;}    table {max-width: 100% !important;width: 100% !important;}    body.single-fayebsg_mobile_apps .banner .royalSlider img.rsImg{object-fit: cover;}    table.ao_tbl_container {    padding: 40px 0;    }    #page-wrap {    margin-top: 0px;}    h2{margin:0; padding:0;}    .mbl_logo{text-align:center;width:100%;float:left;margin:0 0 10px;}    .mbl_logo img{width:150px; margin:10px auto; display:inline-block;}    a.contact_us.link {    margin: 0 auto !important;    max-width: 100px !important;    display: block !important;    }    #copyright .copyright-text{display: none;}    #copyright .copyright-text.copyright_mobile_view{display:block;}    #copyright .copyright-text.copyright_mobile_view{padding: 10px 0 15px;}    #copyright .social-icons ul{margin:0;}    #copyright .social-icons ul li a{width: 32px;height: 32px;}#copyright .copyright-text.copyright_mobile_view {    display: block;    color: #ffffff;    padding: 0 0 10px;    }    #copyright .social-icons ul li a { background-position: bottom center;}    .bottom_inner p{text-align:center;}"//"a,p{text-align:justify}#back-to-top,.header,.top_header,div#intercom-container,div#twitterbar,footer#footer{display:none}.top_header{background:#222;padding:3px 0 0 }.aligncenter{margin:10px 0 30px!important;max-width:100%!important;height:auto!important}.alignleft,img{margin:10px 0 30px;max-width:100%;height:auto}iframe{max-width:100%;height:auto!important;margin-top:10px}p{margin:0 0 20px}.image-1,.jive-image{margin:10px 0 30px!important;max-width:100%!important;height:auto!important}table.j-table{width:100%!important}.tablepress tbody tr td,.tablepress thead tr th{text-align:center}.header .logo{margin:0 auto;float:none}.banner{margin-top:0!important}body div#back-to-top{display:none!important}.bannercontainer.four.next.sugar_crm .content_rgt{width:100%}#new-royalslider-35.royalSlider{height:200px !important}#new-royalslider-35.royalSlider img{object-fit: cover;}.bannercontainer.four.next.sugar_crm .content_rgt p{margin:0;object-fit: cover;}.bannercontainer.four.next.sugar_crm .content_rgt img{margin:10px 0}.formField .formFieldMedium,.formField .formTextAreaWidthMedium{width:100%;min-height:30px;margin:10px 0 0}input[type=button]{border:2px solid #035db8;background-color:#035db8;color:#fff;padding:10px 20px}input,textarea{padding:5px 10px}table{max-width:100%!important;width:100%!important}body.single-fayebsg_mobile_apps .banner .royalSlider img.rsImg{object-fit:cover}table.ao_tbl_container{padding:40px 0}#page-wrap { margin-top: 0px;}#copyright .copyright-text{display: none;} #copyright .copyright-text.copyright_mobile_view{display:block;} #copyright .copyright-text.copyright_mobile_view{padding: 10px 0 15px;} #copyright .social-icons ul{margin:0;} #copyright .social-icons ul li a{width: 32px;height: 32px;} #copyright .copyright-text.copyright_mobile_view { display: block; color: #ffffff; padding: 0 0 10px; }#copyright .social-icons ul li a { background-position: bottom center;}.bottom_inner p{text-align:center;}"
        
        let js = "var style = document.createElement('style'); style.innerHTML = '\(css)'; document.head.appendChild(style);"
        
//        webView.evaluateJavaScript(js, completionHandler: nil)
        
        webView.evaluateJavaScript(js) { (result, error) in
            
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
                webView.isHidden = false
            }
            
            if error != nil {
            }
            else{
                
            }
        }
    }

        
        func setupSpinner(){
            spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height:40))
            spinner.color = UIColor.darkGray
            self.spinner.center = CGPoint(x:UIScreen.main.bounds.size.width / 2, y:UIScreen.main.bounds.size.height / 2)
            self.view.addSubview(spinner)
            spinner.hidesWhenStopped = true
        }
        
        // MARK:- WebView Delegates
        
        /*func webViewDidStartLoad(_ webView: UIWebView) {
            NSLog("Webview started Loading")
            DispatchQueue.main.async {
                self.spinner.startAnimating()
            }
        }
        func webViewDidFinishLoad(_ webView: UIWebView) {
            print("\nWebview did finish load", terminator: "")
            
            let path = Bundle.main.path(forResource:"app", ofType:"css", inDirectory: "Web_Assets/css")
            do {
                let ccssContents = try String(contentsOfFile: path!)
//                let cssContent = try NSString(contentsOfFile: path, encoding: NSUTF8StringEncoding as NSStringEncoding);
                let javaScrStr = "var style = document.createElement('style'); style.innerHTML = '%@'; document.head.appendChild(style)"
                let JavaScrWithCSS = NSString(format: javaScrStr as NSString, ccssContents)
                self.webView.stringByEvaluatingJavaScript(from:JavaScrWithCSS as String)
                
            }
            catch let error as NSError {
                print(error);
            }
            
            
            DispatchQueue.main.async {
                self.spinner.stopAnimating()
//                let path = Bundle.main.path(forResource:"app", ofType:"css", inDirectory: "Web_Assets/css")
////                let path = Bundle.main.pathForResource("styles", ofType: "css")
//                let javaScriptStr = "var link = document.createElement('link'); link.href = '%@'; link.rel = 'stylesheet'; document.head.appendChild(link)"
//                let javaScripthPath = javaScriptStr + path!
//                webView.stringByEvaluatingJavaScript(from: javaScripthPath)
                
//                //Second way
//                do {
//                    let cssContent = try NSString(contentsOfFile: path, encoding: NSUTF8StringEncoding as NSStringEncoding);
//                    let javaScrStr = "var style = document.createElement('style'); style.innerHTML = '%@'; document.head.appendChild(style)"
//                    let JavaScrWithCSS = NSString(format: javaScrStr, cssContent)
//                    self.articleView.stringByEvaluatingJavaScriptFromString(JavaScrWithCSS as String)
//
//                }
//                catch let error as NSError {
//                    print(error);
//                }

            }
        }
        
        func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
            print("WebView Failed")
            DispatchQueue.main.async {
                self.spinner.stopAnimating()
            }
        }
        func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
            
            print("Navigation Url \(request.url!)")
            return true
        }*/
        
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            print("here link Activated!!!")
            
            let url = navigationAction.request.url
            
            let urlStr = url!.absoluteString
            
            UIApplication.shared.open(URL(string : urlStr)!, options: [:], completionHandler: { (status) in
            })
            
            decisionHandler(WKNavigationActionPolicy.cancel)
        }
        else{
            decisionHandler(WKNavigationActionPolicy.allow)
        }
    }
        
        @objc func backAction(sender: UIBarButtonItem) {
            // custom actions here
            if(webView.canGoBack) {
                webView.goBack()
            }
            else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    
    //MARK:- FooterView Methods
    @IBAction func slideShareBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://www.slideshare.net/FayeBusinessSystemsGroup")
    }
    
    @IBAction func twitterBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://twitter.com/FBSG")
    }
    
    @IBAction func facebookBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://www.facebook.com/FayeBusinessSystemsGroup?ref=ts")
    }
    
    @IBAction func googleBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://plus.google.com/103423219342581009269")
    }
    
    @IBAction func linkedinBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://www.linkedin.com/company/684045/")
    }
    
    @IBAction func youtubeBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://www.youtube.com/user/FayeBusinessSystem?feature=mhee")
    }
    
    @IBAction func rssBtnAction(_ sender: Any) {
        openURLString(urlStr: "https://fayebsg.com/feed/")
    }
    
    @IBAction func btnContactusAction(_ sender: UIButton) {
        
        let ContactUsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        ContactUsVC.showNav = true
        self.navigationController?.pushViewController(ContactUsVC, animated: true)
        
    }
    func openURLString(urlStr : String) {
        let url = URL(string: urlStr)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }


}
