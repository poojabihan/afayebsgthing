//
//  GenericMethods.swift
//  TaxApp
//
//  Created by Sanjeev T on 29/12/16.
//  Copyright © 2016 Swift trials. All rights reserved.
//

import UIKit
import Foundation
//import SCrypto
//import Crashlytics

extension String {
    func condenseWhitespace() -> String {
        return self.components(separatedBy: CharacterSet.whitespacesAndNewlines)
            .filter { !$0.isEmpty }
            .joined(separator: " ")
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedStringKey.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedStringKey.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
    
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    
    
    
}

extension UITextField {
    var condenseWhiteSpaceTextField : String
    {
        return (self.text?.condenseWhitespace())!
    }
}

extension UserDefaults {
    func contains(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
}


class GenericMethods: NSObject {
    
//    static let topmostViewcontroller = UIApplication.shared.keyWindow?.rootViewController
    static var roundRectButtonPopTipView: SwiftPopTipView?
    
    //loading
    static var appDelegate = (UIApplication.shared.delegate as! AppDelegate)
    static var bgView : UIView = UIView()
    static var loaderView : UIView = UIView()
    static var activityIndicator = UIActivityIndicatorView()
    //    static var isLoading : Bool = false

    
    class func topMostController() -> UIViewController
    {
        var topController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
        while ((topController?.presentedViewController) != nil) {
            topController = topController?.presentedViewController
        }
        return topController!
    }
    
    class func isAlertPresent() -> Bool
    {
        let topController: UINavigationController? = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
        
        if topController?.visibleViewController is UIAlertController{
            return true
        }
        else{
            return false
        }
        
        
    }

    //MARK: AlertController
    class func showAlert(alertMessage: String)
    {
        let alert = UIAlertController(title: nil, message: alertMessage, preferredStyle:
            .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
        
        DispatchQueue.main.async {
            topMostController().present(alert, animated: true, completion: nil)
            GenericMethods.hideLoading() // Hide any loader presented
        }
    }
    
    class func showAlertWithTitle(alertTitleStr : String,alertMessage: String)
    {
        let alert = UIAlertController(title: alertTitleStr, message: alertMessage, preferredStyle:
            .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
        

       
            DispatchQueue.main.async {
                topMostController().present(alert, animated: true, completion: nil)
                GenericMethods.hideLoading() // Hide any loader presented
            }
        
     
        
        
    }

    
    //MARK: AlertController
    class func showYesOrNoAlertWithCompletionHandler(alertTitle: String?,alertMessage: String, completionHandlerForOk : @escaping (UIAlertAction) -> Void)
    {
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle:
            .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default, handler: completionHandlerForOk))
        alert.addAction(UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel, handler: nil))

        DispatchQueue.main.async {
            topMostController().present(alert, animated: true, completion: nil)
            GenericMethods.hideLoading() // Hide any loader presented
        }
    }

    class func showAlertWithCompletionHandler(title: String ,alertMessage: String, completionHandlerForOk : @escaping (UIAlertAction) -> Void)
    {
        let alert = UIAlertController(title: title, message: alertMessage, preferredStyle:
            .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: completionHandlerForOk))


        DispatchQueue.main.async {
            topMostController().present(alert, animated: true, completion: nil)
            GenericMethods.hideLoading() // Hide any loader presented
        }
    }

    //MARK:ActionSheet
    
    
    
    class func showActionSheet(actions: [String]) {
        let alert = UIAlertController(title: NSLocalizedString("Greetings!", comment: ""), message: nil, preferredStyle:
            .actionSheet)
        
        for stringIndex in actions {
            
            alert.addAction(UIAlertAction(title: stringIndex, style: .default, handler: nil))
            
        }
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        DispatchQueue.main.async {
            topMostController().present(alert, animated: true, completion: nil)
        }
    }

    class func resizeImage (image: UIImage) -> UIImage {

        var image : UIImage = image

        let imageWidth: Double = Double(image.size.width)
        let imageHeight: Double = Double(image.size.height)
        var compressionRatio: Double

        if imageWidth >= 3000 || imageHeight >= 3000 {
            compressionRatio = 3
        }
        else if imageWidth >= 2000 || imageHeight >= 2000 {
            compressionRatio = 2
        }
        else if imageWidth >= 1500 || imageHeight >= 1500 {
            compressionRatio = 1.5
        }
        else if imageWidth >= 1250 || imageHeight >= 1250 {
            compressionRatio = 1.25
        }
        else {
            compressionRatio = 1
        }

        let newSize = CGSize(width: Double(image.size.width) / compressionRatio, height: Double(image.size.height) / compressionRatio)
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))

        image =   UIGraphicsGetImageFromCurrentImageContext()!

        return image
    }


    class func showNoInternetAlert()
    {
        let alert = UIAlertController(title: NSLocalizedString("No Internet Connection", comment: ""), message: NSLocalizedString("Check your network settings and try again", comment: ""), preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(action:UIAlertAction) in
        })
        
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default, handler:{ (action:UIAlertAction) in
            guard let settingsUrl = URL(string: "App-Prefs:root=Settings")
                else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl)
            {
                if #available(iOS 10.0, *)
                {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Back to app From Settings: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                    UIApplication.shared.openURL(settingsUrl)
                }
            }
        })
        
        alert.addAction(okAction)
        alert.addAction(settingsAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    class func showVersionUpdateAlert()
    {
        let alert = UIAlertController(title: NSLocalizedString("Update the MyUnitBuzz App", comment: ""), message: NSLocalizedString("To access our new features, please download the latest version of our app", comment: ""), preferredStyle: .alert)
       
        
        let settingsAction = UIAlertAction(title: NSLocalizedString("Take me to App Store", comment: ""), style: .default, handler:{ (action:UIAlertAction) in
            let url:URL = URL(string: Config.AppstoreUrl)!
            UIApplication.shared.openURL(url)
            
            
        })
        
//        let okAction = UIAlertAction(title: "Cancel", style: .default, handler: {(action:UIAlertAction) in
//        })
//        alert.addAction(okAction)
        alert.addAction(settingsAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    class func convertToCurrencyFormat(amount:Double) -> String
    {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US")
        if #available(iOS 9.0, *) {
            formatter.numberStyle = .currencyAccounting
        } else {
            // Fallback on earlier versions
            formatter.numberStyle = .currency
        }
        formatter.roundingMode = .ceiling
        formatter.minimumIntegerDigits = 1
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        return formatter.string(from: NSNumber(value:amount))!
    }
    class func convertToCurrencyFormatWithoutSymbol(amount:Double) -> String
    {
        let formatter = NumberFormatter()
        formatter.roundingMode = .ceiling
        formatter.minimumIntegerDigits = 1
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        return formatter.string(from: NSNumber(value:amount))!
    }
    
    class func convertToPercentageFormatWithoutSymbol(amount:Double) -> String
    {
        let formatter = NumberFormatter()
        formatter.roundingMode = .ceiling
        formatter.minimumIntegerDigits = 1
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        return formatter.string(from: NSNumber(value:amount))!
    }
    
    class func roundOffToCeiling(amount:Double) -> String
    {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.roundingMode = .ceiling
        formatter.minimumIntegerDigits = 1
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        return formatter.string(from: NSNumber(value:amount))!
    }
    class func roundOffToCeilingQuantity(amount:Double) -> String
    {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.roundingMode = .ceiling
        formatter.minimumIntegerDigits = 1
        formatter.maximumFractionDigits = 3
        formatter.minimumFractionDigits = 3
        return formatter.string(from: NSNumber(value:amount))!
    }

    
    class func getTimeStamp() -> String
    {
        let formatter = DateFormatter()
        formatter.locale =  NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
        let timeStamp: String = formatter.string(from: Date())
        return timeStamp
    }
    
    class func addressFormatter(address1: String, address2: String, city: String, state: String, country: String, zipCode: String) -> String
    {
        var formattedAddress: String = ""
        var combinedAddressLine: String = ""
        
        let address1Str = Validation.removeNullFromString(address1)
        let address2Str = Validation.removeNullFromString(address2)
        let cityStr = Validation.removeNullFromString(city)
        let stateStr = Validation.removeNullFromString(state)
        let countryStr = Validation.removeNullFromString(country)
        let zipCodeStr = Validation.removeNullFromString(zipCode)
        
        if address2Str == ""
        {
            combinedAddressLine = address1Str
        }
        else
        {
            combinedAddressLine = address1Str + ", " + address2Str
        }
        
        if countryStr == "USA"
        {
            formattedAddress = "\(combinedAddressLine), \(cityStr), \(stateStr) \(zipCodeStr)"
        }
        else
        {
            formattedAddress = "\(combinedAddressLine), \(cityStr), \(stateStr), \(countryStr) - \(zipCodeStr)"
        }
        
        formattedAddress = (formattedAddress as NSString).replacingOccurrences(of: " +", with: " ", options: .regularExpression, range: NSRange(location: 0, length: formattedAddress.count))
        formattedAddress = (formattedAddress as NSString).replacingOccurrences(of: "(null)", with: "", options: .regularExpression, range: NSRange(location: 0, length: formattedAddress.count))
        formattedAddress = (formattedAddress as NSString).replacingOccurrences(of: "<null>", with: "", options: .regularExpression, range: NSRange(location: 0, length: formattedAddress.count))

        return formattedAddress
    }
    
    class func addPercentEncoding(string: String) -> String
    {
        let allowedCharacterSet = (CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted)
        
        let encodedString = string.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)!
        
        return Validation.removeNullFromString(encodedString)
    }
    
    class func showErrorPopup(errorMessage: String, viewForShowingError: AnyObject)
    {
        if let _ = roundRectButtonPopTipView {
            // Dismiss
            roundRectButtonPopTipView?.dismissAnimated(true)
            roundRectButtonPopTipView = nil
        } else {
            roundRectButtonPopTipView = SwiftPopTipView(message: errorMessage)
            roundRectButtonPopTipView?.popColor = UIColor.clear
            
            roundRectButtonPopTipView?.textColor = UIColor.red
            
            roundRectButtonPopTipView?.presentPointingAtView(viewForShowingError as! UIView, containerView: (topMostController().view)!, animated: true)
        }
    }
    
    class func showLoading()
    {
        if appDelegate.isLoading == false
        {
            appDelegate.isLoading = true
            DispatchQueue.main.async {
                bgView = UIView(frame: CGRect(x: 0, y: 0, width: (appDelegate.window?.frame.size.width)!, height: (appDelegate.window?.frame.size.height)!))
                bgView.backgroundColor = UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.5)
                appDelegate.window?.addSubview(bgView)
                appDelegate.window?.bringSubview(toFront: bgView)
                
                loaderView = UIView(frame: CGRect(x: (bgView.frame.size.width - 100)/2 , y: (bgView.frame.size.height - 100)/2 , width: 100, height: 100))
                loaderView.backgroundColor = UIColor.black
                loaderView.layer.cornerRadius = 15
                loaderView.layer.masksToBounds = true
                appDelegate.window?.addSubview(loaderView)
                
                activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
                activityIndicator.frame = CGRect(x: (loaderView.frame.size.width - 50)/2 , y: (loaderView.frame.size.height - 50)/2 , width: 50, height: 50)
                activityIndicator.startAnimating()
                loaderView.addSubview(activityIndicator)
            }
        }
    }
    class func hideLoading()
    {
        if appDelegate.isLoading == true
        {
            appDelegate.isLoading = false
            DispatchQueue.main.async {
                activityIndicator.removeFromSuperview()
                bgView.removeFromSuperview()
                loaderView.removeFromSuperview()
            }
        }
    }
    
    class func clearUserIdFromDefaults()
    {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "UserId")
        defaults.removeObject(forKey: "AccessToken")
        defaults.synchronize()
    }
    
//    //Encryption and Decryption
//    class func MD5Encryption(stringToBeEncrypted: String) -> String
//    {
//        print("stringToBeEncrypted..\(stringToBeEncrypted)")
//
//        let plaintext = stringToBeEncrypted.data(using: String.Encoding.utf8)!
//
//        let digestKey = "Pay*Wow_2017$Crypto#Key"
//        let sharedSecretKey = digestKey.data(using: String.Encoding.utf8)!.MD5() // AES-256
//
//        let ciphertext = try! plaintext.encrypt(.aes, options: [.PKCS7Padding, .ECBMode], key: sharedSecretKey)
//        let encryptedString = ciphertext.base64EncodedString()
//
//        return encryptedString
//    }
//
//    class func decrypt(stringToBeDecrypted: String) -> String
//    {
//        let encryptedData = Data.init(base64Encoded: stringToBeDecrypted)!
//
//        let digestKey = "Pay*Wow_2017$Crypto#Key"
//        let sharedSecretKey = digestKey.data(using: String.Encoding.utf8)!.MD5() // AES-256
//
//        let decrpytedText = try! encryptedData.decrypt(.aes, options: [.PKCS7Padding, .ECBMode], key: sharedSecretKey)
//        let decryptedString = String(data: decrpytedText, encoding: .utf8)!
//
//        return decryptedString
//    }
//
//    class func logUserDataInCrashlytics() {
//        // TODO: Use the current user's information
//        // You can call any combination of these three methods
//
//        let emailAddress: String = UserDefaults.standard.object(forKey: "EmailAddress") as! String
//        let userId: String = UserDefaults.standard.object(forKey: "UserId") as! String
//        let contactName: String = Validation.removeNullFromString(UserDefaults.standard.object(forKey: "FirstName") as! String) + Validation.removeNullFromString(UserDefaults.standard.object(forKey: "LastName") as! String)
//
//        Crashlytics.sharedInstance().setUserEmail(emailAddress)
//        Crashlytics.sharedInstance().setUserIdentifier(userId)
//        Crashlytics.sharedInstance().setUserName(contactName)
//    }
}
