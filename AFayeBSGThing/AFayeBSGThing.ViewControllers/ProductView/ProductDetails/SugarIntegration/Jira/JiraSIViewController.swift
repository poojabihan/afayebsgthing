//
//  JiraSIViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/9/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class JiraSIViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var youtubeView: YTPlayerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "Jira"
        
        self.automaticallyAdjustsScrollViewInsets = false
        
       // youtubeView.load(withVideoId: "r8ZCBZLFBR0")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: ScreenUtils.CurrentDeviceWidth, height: contactView.frame.origin.y + contactView.frame.size.height + 10)
    }
    
    @IBAction func contactBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Contact")
    }
    
    @IBAction func spechSheetBtnAction(_ sender: Any) {
         pushToQuickBooksDetails(type: "SpechSheet")
        
    }
    @IBAction func supoortBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Support")
    }
    
    @IBAction func phoneNumberBtnAction(_ sender: Any) {
        guard let number = URL(string: "tel://" + "8182804820") else { return }
        UIApplication.shared.open(number)
    }
    
    func pushToQuickBooksDetails(type : String) {
        let jiraSIWebViewVC = self.storyboard?.instantiateViewController(withIdentifier: "JiraSIWebViewVC") as! JiraSIWebViewVC
        jiraSIWebViewVC.typeOfStr = type
        self.navigationController?.pushViewController(jiraSIWebViewVC, animated: true)
    }

}
