//
//  DashboardManagerSEViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/9/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import youtube_ios_player_helper



class DashboardManagerSEViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var contactView: UIView!
    
    @IBOutlet weak var youtubeView: YTPlayerView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "DashBoard Manager"
        self.automaticallyAdjustsScrollViewInsets = false
        
        youtubeView.load(withVideoId: "Q7r0q6xwMuU")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: ScreenUtils.CurrentDeviceWidth, height: contactView.frame.origin.y + contactView.frame.size.height + 10)
    }
    @IBAction func contactUsForMoreInfoBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "ContactUDashboardManager")
    }
    
    @IBAction func specSheetBtnAction(_ sender: Any) {
        if let url = URL(string: "https://campaign.fayebsg.com/acton/attachment/9763/f-02a9/1/-/-/-/-/SugarCRM%20Dashboard%20Manager%20-%20FayeBSG.pdf") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    @IBAction func supportBtnAction(_ sender: Any) {
        if let url = URL(string: "https://campaign.fayebsg.com/acton/attachment/9763/f-01eb/1/-/-/-/-/Annual%20License%20Support%20for%20FayeBSG.pdf") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction func phoneNumberBtnAction(_ sender: Any) {
        guard let number = URL(string: "tel://" + "8182804820") else { return }
        UIApplication.shared.open(number)
    }
    
    func pushToQuickBooksDetails(type : String) {
        let sugarMobileSEWebViewVC = self.storyboard?.instantiateViewController(withIdentifier: "SugarMobileSEWebViewVC") as! SugarMobileSEWebViewVC
        sugarMobileSEWebViewVC.typeOfStr = type
        self.navigationController?.pushViewController(sugarMobileSEWebViewVC, animated: true)
    }
    
}
