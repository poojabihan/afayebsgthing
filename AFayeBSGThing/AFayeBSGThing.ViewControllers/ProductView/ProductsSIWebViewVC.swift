//
//  ProductsSIWebViewVC.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/26/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import WebKit
class ProductsSIWebViewVC: UIViewController, UIWebViewDelegate, WKNavigationDelegate  {
    
//    @IBOutlet weak var webView: UIWebView!
    var webView: WKWebView!

    var spinner: UIActivityIndicatorView!
    var urlStr : String = ""
    var navigationTitleStr : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: CGRect(x: 0 , y: 0 , width: self.view.frame.size.width  , height: self.view.frame.size.height), configuration: webConfiguration)
//        webView.uiDelegate = self
        webView.navigationDelegate = self
        //webView.sizeToFit()
        
        self.view.addSubview(webView)
        
        self.navigationItem.title = navigationTitleStr
        self.automaticallyAdjustsScrollViewInsets = false
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "backArrow") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
        
        setupSpinner()
        spinner.startAnimating()
        var url : URL!
        url = URL(string: urlStr)!
//        webView.scalesPageToFit = true
        webView.contentMode = .scaleAspectFit
        webView.load(URLRequest(url: url))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupSpinner(){
        spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height:40))
        spinner.color = UIColor.darkGray
        self.spinner.center = CGPoint(x:UIScreen.main.bounds.size.width / 2, y:UIScreen.main.bounds.size.height / 2)
        self.view.addSubview(spinner)
        spinner.hidesWhenStopped = true
    }
    
    // MARK:- WebView Delegates
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        NSLog("Webview started Loading")
        DispatchQueue.main.async {
            self.spinner.startAnimating()
        }
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("\nWebview did finish load", terminator: "")
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("didFinish navigation:");
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
        }
    }

    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("WebView Failed")
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
        }
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        print("Navigation Url \(request.url!)")
        return true
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print("Navigation Url \(navigationAction.request.url!)")
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            print("here link Activated!!!")
            
            let url = navigationAction.request.url
            
            let urlStr = url!.absoluteString
            
                UIApplication.shared.open(URL(string : urlStr)!, options: [:], completionHandler: { (status) in
                    
                })
            
            decisionHandler(WKNavigationActionPolicy.cancel)
        }
        else{
            decisionHandler(WKNavigationActionPolicy.allow)
        }
    }


    
    @objc func backAction(sender: UIBarButtonItem) {
        // custom actions here
        if(webView.canGoBack) {
            webView.goBack()
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
    }

}
