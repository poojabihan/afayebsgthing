//
//  RegistrationView.swift
//  FayeBSG
//
//  Created by Apple on 03/11/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class RegistrationView: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnSubmit: UIButton!

    var textField: UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(PageViewController.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PageViewController.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        self.hideKeyboardWhenTappedAround()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        btnSubmit.layer.cornerRadius = 20
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func btnSaveAction(_ sender: Any) {
        
        if (txtFirstName.text?.isEmpty)! {
            
            let alert = UIAlertController(title: nil, message: "Please enter First Name.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }
        else if (txtLastName.text?.isEmpty)! {
            
            let alert = UIAlertController(title: nil, message: "Please enter Last Name.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }
        else if (txtEmail.text?.isEmpty)! {
            
            let alert = UIAlertController(title: nil, message: "Please enter Email.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }
        else if (txtEmail.text!.isValidEmail() == false){
            
            let alert = UIAlertController(title: nil, message: "Please enter valid Email.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }
        else if (txtPassword.text?.isEmpty)! {
            
            let alert = UIAlertController(title: nil, message: "Please enter Password.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }
        else if(checkForSpace(strCheckString: txtPassword.text!) ==  true){
            
            let alert = UIAlertController(title: nil, message: "White space not allowed in the password field.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
        }
        else{
            
            callWebServiceToRegister()
        }
    }
    
    func callWebServiceToRegister(){
        
        GenericMethods.showLoading()
        let param =
            ["fname":txtFirstName.text ?? "",
             "lname":txtLastName.text ?? "" ,
             "password":txtPassword.text ?? "",
             "email":txtEmail.text ?? "",
             ] as [String : Any]
        
        print(param)
        
        WebAPIHelper.postMethod(params: param, methodName: "/ApiHome.php?method=register", success: { (response) in
            
            print("Response \(response!)")
            DispatchQueue.main.async{
                GenericMethods.hideLoading()
            }
            
            if !(response is NSNull) {
                
                let alert = UIAlertController(title: nil, message: response?["message"] as? String, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (ok) in
                    self.btnCancelAction(self)
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                DispatchQueue.main.async {
                    
                self.present(alert, animated: true)
                
                }
                /* UserDefaults.standard.set("Yes", forKey: "LoggedIn") //Bool
                 UserDefaults.standard.set(txtEmail, forKey: "Email") //Bool
                 
                 DispatchQueue.main.async {
                 self.loginView.isHidden = true
                 }*/
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
            
        }) { (error) in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        }
        
    }
    
    func checkForSpace(strCheckString : String) -> Bool {
        
        let whiteSpace = " "
        if strCheckString.contains(whiteSpace) {
            print(strCheckString)
            print ("has whitespace")
            return true
        } else {
            print(strCheckString)
            print("no whitespace")
            return false
        }
        
    }
    
    //MARK: - UITextFiled keyboard hide unhide methods
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    
    //MARK: - UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
}
extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}
