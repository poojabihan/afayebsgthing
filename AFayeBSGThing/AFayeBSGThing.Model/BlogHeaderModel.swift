//
//  BlogHeaderModel.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/1/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import ObjectMapper

class BlogHeaderModel: Mappable {

    var dataFeed = [dataInside]()
    var status : Bool = false
    var message : String = ""
    var service_name : String = ""
    
    
    required init?(map: Map)
    {
        
    }
    init()
    {
        
        
    }
    func mapping(map: Map)
    {
        
        status <- map["status"]
        message <- map["message"]
        dataFeed <- map["result"]
        service_name <- map["service_name"]
        
    }
    
}

class dataInside: Mappable
{
    var name :String = ""
    var id :String = ""
   
    
    
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    func mapping(map: Map)
    {
        name                   <- map["name"]
        id                    <- map["id"]
        
        
        
        
    }
    
}
