//
//  QuickBooksSIViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/5/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class QuickBooksSIViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var youtubeView: YTPlayerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Quick Books"
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
        youtubeView.load(withVideoId: "h8AEAXrdPfg")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: ScreenUtils.CurrentDeviceWidth, height: contactView.frame.origin.y + contactView.frame.size.height + 10)
    }

    @IBAction func contactusBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Contact")
    }
    
    @IBAction func specSheetBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Specsheet")
    }
    
    @IBAction func versionMatrixBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "VersionMatrix")
    }
    
    @IBAction func prof_OnlineEditionBuyNowBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "OnlineEdition")
    }
    
    @IBAction func advancedEditionBuyNowBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "AdvancedEdition")
    }
    
    @IBAction func enterpriseEditionBuyNowBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "EnterprisEEdition")
    }
    
    @IBAction func phoneNumberBtnAction(_ sender: Any) {
        guard let number = URL(string: "tel://" + "8182804820") else { return }
        UIApplication.shared.open(number)
    }
    
    func pushToQuickBooksDetails(type : String) {
        let quickBooksSIWebViewVC = self.storyboard?.instantiateViewController(withIdentifier: "QuickBooksSIWebViewVC") as! QuickBooksSIWebViewVC
        quickBooksSIWebViewVC.typeOfStr = type
        self.navigationController?.pushViewController(quickBooksSIWebViewVC, animated: true)
    }
}
