//
//  Colors.swift
//  Express1099
//
//  Created by STS-247 on 10/04/17.
//  Copyright © 2017 SPAN Technology. All rights reserved.
//

import UIKit

struct Colors
{
    static let THEMECOLOR = UIColor(red: 57/255, green: 121/255, blue: 200/255, alpha: 1.0)
    static let THEMECOLORALPHA = UIColor(red: 230/255, green: 10/255, blue: 100/255, alpha: 1.0)
    static let PRIMARYCOLOR = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1.0)
    static let SECONDARYCOLOR = UIColor(red: 128/255, green: 128/255, blue: 128/255, alpha: 1.0)

    static let LIGHTTEXTCOLOR = UIColor(red: 117/255, green: 117/255, blue: 117/255, alpha: 1.0)
    static let DARKTEXTCOLOR = UIColor(red: 57/255, green: 57/255, blue: 57/255, alpha: 1.0)
    static let BLUECOLOR = UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 1.0)

    static let GREENBUTTONCOLOR = UIColor(red: 76/255, green: 182/255, blue: 108/255, alpha: 1.0)

    static let CELLSECTIONBACKGROUNDCOLOR = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
    
    
    

    func generateColors(forPieChart : ([Double], [Double], [Double])) -> [UIColor]
    {

        return [UIColor.red]
    }

    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
