//
//  AuthorizenetSIViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/9/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class AuthorizenetSIViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var youtubeView: YTPlayerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "Authorize.net"
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        youtubeView.load(withVideoId: "Yu9y7To--_o")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: ScreenUtils.CurrentDeviceWidth, height: contactView.frame.origin.y + contactView.frame.size.height + 10)
    }
    
    @IBAction func contactBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Contact")
    }
    
    @IBAction func authorizeIntegration_BtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "AuthorizeIntegration")
    }
    @IBAction func supoortBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Support")
    }
    
    @IBAction func professionalEdtion_BtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "ProfessionalEditionBuyNow")
    }
    
    @IBAction func enterpriseEdition_BtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "EnterpriseEditionBuyNow")
    }
    @IBAction func phoneNumberBtnAction(_ sender: Any) {
        guard let number = URL(string: "tel://" + "8182804820") else { return }
        UIApplication.shared.open(number)
    }
    func pushToQuickBooksDetails(type : String) {
        let authorizeNetSIWebViewVC = self.storyboard?.instantiateViewController(withIdentifier: "AuthorizenetSIWebViewVC") as! AuthorizenetSIWebViewVC
        authorizeNetSIWebViewVC.typeOfStr = type
        self.navigationController?.pushViewController(authorizeNetSIWebViewVC, animated: true)
    }

}
