//
//  ForgotPasswordViewController.swift
//  FayeBSG
//
//  Created by kdstudio on 20/11/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        btnSubmit.layer.cornerRadius = 20
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSubmitAction() {
        if (txtEmail.text?.isEmpty)! {
            
            let alert = UIAlertController(title: nil, message: "Please enter Email.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }
        else if (txtEmail.text!.isValidEmail() == false){
            
            let alert = UIAlertController(title: nil, message: "Please enter valid Email.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }
        else{
            webServiceCallingForForgotPassword()
        }
    }
    
    @IBAction func btncloseAction() {
        self.dismiss(animated: false, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func webServiceCallingForForgotPassword()
    {
        
        GenericMethods.showLoading()
        WebAPIHelper.postMethod(params: ["email" : txtEmail.text ?? ""], methodName: "/ApiHome.php?method=forgotPassword", success: { (response) in
            
            print("Response \(response!)")
            DispatchQueue.main.async{
                GenericMethods.hideLoading()
            }
            
            if !(response is NSNull) {
                DispatchQueue.main.async{
                    
                    var msg = response?["service_name"] as? String
                    if (response?["message"] as? String == "Success")
                    {
                        msg = "Link to reset password has been sent to your email."
                    }
                    let alert = UIAlertController(title: response?["message"] as? String, message: msg, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        if (response?["message"] as? String == "Success")
                        {
                            self.dismiss(animated: false, completion: nil)
                                                    }
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
            
        }) { (error) in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
