//
//  CustomShareView.swift
//  FayeBSG
//
//  Created by kdstudio on 27/06/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import Social

class CustomShareView: UIView {

    var obj = ViewController()
    var urlString = String()

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @IBAction func btnMailAction(_ sender: Any) {
        print("Mail Clicked")
    }
    @IBAction func btnLinkedInAction(_ sender: Any) {
        print("LinkedIn Clicked")
    }
    @IBAction func btnTwitterAction(_ sender: Any) {
        print("Twitter Clicked")
//        obj.btnTwitterShare(urlString: urlString)
    }
    @IBAction func btnFBAction(_ sender: Any) {
        print("FB Clicked")
//        obj.btnFBAction(urlString: urlString)
    }
}
