//
//  HomeSectionThreeModel.swift
//  FayeBSG
//
//  Created by kdstudio on 27/09/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import ObjectMapper

class HomeSectionThreeModel: Mappable {
    
    
    var dataFeed = [resultThree]()
    var status : Bool = false
    var message : String = ""
    
    
    required init?(map: Map)
    {
        
    }
    init()
    {
        
        
    }
    func mapping(map: Map)
    {
        dataFeed <- map["result"]
        status <- map["status"]
        message <- map["message"]
        
        
    }
    
    
}

class resultThree: Mappable
{
    var home_section_three_image :String = ""
    var home_section_three_text :String = ""
    var home_section_three_button_text :String    = ""
    var home_section_three_button_link :String = ""
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    func mapping(map: Map)
    {
        home_section_three_image                   <- map["home_section_three_image"]
        home_section_three_text                    <- map["home_section_three_text"]
        home_section_three_button_text                <- map["home_section_three_button_text"]
        home_section_three_button_link                 <- map["home_section_three_button_link"]
        
    }
    
}

