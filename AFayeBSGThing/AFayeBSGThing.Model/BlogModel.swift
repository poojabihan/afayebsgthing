//
//  BlogModel.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/1/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import ObjectMapper

class BlogModel: Mappable {

    var dataFeed = [blogData]()
    var status : Bool = false
    var message : String = ""
    var service_name : String = ""
    
    
    required init?(map: Map)
    {
        
    }
    init()
    {
        
        
    }
    func mapping(map: Map)
    {
        
        status <- map["status"]
        message <- map["message"]
        dataFeed <- map["result"]
        service_name <- map["service_name"]
    }
}

class blogData: Mappable
{
    var image :String = ""
    var post_id :String = ""
    var title :String = ""
    var content :String = ""
    var category :String = ""
    var link :String = ""

    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    func mapping(map: Map)
    {
        image                   <- map["image"]
        post_id                    <- map["post_id"]
        title                    <- map["title"]
        content                    <- map["content"]
        category                    <- map["category"]
        link                    <- map["link"]
    }    
}
