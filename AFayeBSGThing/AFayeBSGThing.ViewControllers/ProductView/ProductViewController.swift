//
//  ProductViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 4/28/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    //MARK:- Variable declaration
 
    var sugarIntegrationModelArray  : SugarIntegrationModel = SugarIntegrationModel()
    var sugarEnhancementModelArray  : SugarIntegrationModel = SugarIntegrationModel()
    
    //MARK:- IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib(nibName: "ProductCollectionViewCell", bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: "ProductCollectionViewCell")
//        getSugarIntegrationAPI()
//        getSugarEnhancementsAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getSugarIntegrationAPI()
        getSugarEnhancementsAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getSugarIntegrationAPI()
    {
        GenericMethods.showLoading()
        
        WebAPIHelper.getMethod(methodName: "/ApiProduct.php?method=sugar_integration", success: {
            (response)->Void in
            
            print("getSugarIntegrationAPI Response \(response!)")
           
            
            if !(response is NSNull)
            {
                self.sugarIntegrationModelArray = Parser.parseSugarIntegrationModel(apiResponse: response!)
                
                
                if self.sugarIntegrationModelArray.message == "Success"{
                    
                    
                   
                    
                }
                DispatchQueue.main.async{
                    GenericMethods.hideLoading()
                    self.collectionView.reloadData()
                    
                }
                
                
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
        }, Failure: {
            (error)->Void in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        })
        
    }
   
    func getSugarEnhancementsAPI()
    {
        GenericMethods.showLoading()
        
        WebAPIHelper.getMethod(methodName: "/ApiProduct.php?method=sugar_enhancement", success: {
            (response)->Void in
            
            print("getSugarEnhancementsAPI Response \(response!)")
           
            
            if !(response is NSNull)
            {
                
                
               
                self.sugarEnhancementModelArray = Parser.parseSugarIntegrationModel(apiResponse: response!)
                
                
                if self.sugarEnhancementModelArray.message == "Success"{
                    
                    
                    
                }
                
                DispatchQueue.main.async{
                    GenericMethods.hideLoading()
                    self.collectionView.reloadData()
                    
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
        }, Failure: {
            (error)->Void in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        })
        
    }
    

    //MARK:- CollectionView Delegates
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return sugarIntegrationModelArray.dataFeed.count
        }
        else{
            return sugarEnhancementModelArray.dataFeed.count
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as! ProductCollectionViewCell
        if indexPath.section == 0 {
            
            cell.productImageView.sd_setImage(with: URL(string: self.sugarIntegrationModelArray.dataFeed[indexPath.row].image as String), placeholderImage: #imageLiteral(resourceName: "placeholder_Image"))
            
        }
        else{
            
            cell.productImageView.sd_setImage(with: URL(string: self.sugarEnhancementModelArray.dataFeed[indexPath.row].image as String), placeholderImage: #imageLiteral(resourceName: "placeholder_Image"))
            
        }
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0{
            return CGSize(width:collectionView.frame.size.width, height:179.0)
        }
        else{
            return CGSize(width:collectionView.frame.size.width, height:44.0)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat((collectionView.frame.size.width / 2) - 10), height: CGFloat(80))
    }

    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if section == 1{
            return CGSize(width:collectionView.frame.size.width, height:120.0)
        }
        return CGSize(width:collectionView.frame.size.width, height:0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            
            if indexPath.section == 0 {
                let sectionHeaderOneCellnib = UINib(nibName: "SectionHeaderOneCell", bundle: nil)
                collectionView.register(sectionHeaderOneCellnib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "SectionHeaderOneCell")
                
                let headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeaderOneCell", for: indexPath) as? SectionHeaderOneCell
                
                return headerCell!
            }
            else{
                
                let sectionHeaderTwoCellnib = UINib(nibName: "SectionHeaderTwoCell", bundle: nil)
                collectionView.register(sectionHeaderTwoCellnib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "SectionHeaderTwoCell")
                
                let headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeaderTwoCell", for: indexPath) as? SectionHeaderTwoCell
                
                return headerCell!
            }
            
        case UICollectionElementKindSectionFooter:
            
            if indexPath.section == 1 {
                let sectionFooterCellnib = UINib(nibName: "SectionFooterCell", bundle: nil)
                collectionView.register(sectionFooterCellnib, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "SectionFooterCell")
                
            
                let footerCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionFooterCell", for: indexPath) as? SectionFooterCell
                // btn.addTarget(self, action:#selector(SCLAlertView.buttonTapped(_:)), for:.touchUpInside)
                
               footerCell?.btnContactUs.addTarget(self, action: #selector(self.pressContactButton(_:)), for: .touchUpInside) //<- use `#selector(...)`
                
                return footerCell!
            }
            
        default:
            
            assert(false, "Unexpected element kind")
        }
        
        
        
        return UICollectionReusableView()
        
    }

    //The target function
    @objc func pressContactButton(_ sender: UIButton){ //<- needs `@objc`
        print("\(sender)")
        
        let ContactUsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        ContactUsVC.showNav = true
        self.navigationController?.pushViewController(ContactUsVC, animated: true)
        
    }
    
    

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var urlStr : String = ""
        var navigationTitleStr : String = ""
        
        if indexPath.section == 0 {
            urlStr = self.sugarIntegrationModelArray.dataFeed[indexPath.row].link
            navigationTitleStr = self.sugarIntegrationModelArray.dataFeed[indexPath.row].title;
        }
        else{
            urlStr = self.sugarEnhancementModelArray.dataFeed[indexPath.row].link;
            navigationTitleStr = self.sugarEnhancementModelArray.dataFeed[indexPath.row].title;
        }
        /*if indexPath.section == 0{
            if self.sugarIntegrationModelArray.dataFeed[indexPath.row].title == "SugarCRM QuickBooks Integration Application" {
               urlStr = "https://fayebsg.com/fayebsg_mobile_apps/sugarcrm-quickbooks-integration/"
                navigationTitleStr = "Quick Books"
            }
            else if self.sugarIntegrationModelArray.dataFeed[indexPath.row].title == "SugarCRM Dropbox Integration" {
                
                urlStr = "https://fayebsg.com/fayebsg_mobile_apps/sugarcrm-dropbox-integration/"
                navigationTitleStr = "Drop Box"
            }
            else if self.sugarIntegrationModelArray.dataFeed[indexPath.row].title == "SugarCRM Sage Integration Application" {
                urlStr = "https://fayebsg.com/fayebsg_mobile_apps/sugarcrm-sage-integration/"
                navigationTitleStr = "Sage"
                
            }
            else if self.sugarIntegrationModelArray.dataFeed[indexPath.row].title == "SugarCRM Constant Contact Integration Application" {
                urlStr = "https://fayebsg.com/fayebsg_mobile_apps/sugarcrm-constant-contact-integration/"
                navigationTitleStr = "Constant Contact"
                
            }else if self.sugarIntegrationModelArray.dataFeed[indexPath.row].title == "SugarCRM Intacct Integration Application" {
                urlStr = "https://fayebsg.com/fayebsg_mobile_apps/sugarcrm-intacct-integration/"
                navigationTitleStr = "Intacct"
                
            }else if self.sugarIntegrationModelArray.dataFeed[indexPath.row].title == "SugarCRM Box Integration Application" {
                urlStr = "https://fayebsg.com/fayebsg_mobile_apps/sugarcrm-box-integration/"
                navigationTitleStr = "Box"
                
            }
            else if self.sugarIntegrationModelArray.dataFeed[indexPath.row].title == "SugarCRM Jira Integration Application" {
                
                urlStr = "https://fayebsg.com/fayebsg_mobile_apps/sugarcrm-jira-integration/"
                navigationTitleStr = "Jira"
                
            }
            else if self.sugarIntegrationModelArray.dataFeed[indexPath.row].title == "SugarCRM Authorize.Net Integration Application" {
                
                urlStr = "https://fayebsg.com/fayebsg_mobile_apps/sugarcrm-authorize-net-integration/"
                navigationTitleStr = "Authorize.net"

                
            }
        }
        else
        {
            if self.sugarEnhancementModelArray.dataFeed[indexPath.row].title == "Visual Property Manager" {
                urlStr = "https://fayebsg.com/fayebsg_mobile_apps/visual-property-manager/"
                navigationTitleStr = "Visual Property Manager"
            }
            else if self.sugarEnhancementModelArray.dataFeed[indexPath.row].title == "Sugar Mobile e" {
                
                
                urlStr = "https://fayebsg.com/fayebsg_mobile_apps/sugar-mobile-e/"
                navigationTitleStr = "Sugar Mobile e"
            }
            else if self.sugarEnhancementModelArray.dataFeed[indexPath.row].title == "CASH: Customer Access Sugar Hub" {
                urlStr = "https://fayebsg.com/fayebsg_mobile_apps/sugarcrm-hosting/"
                navigationTitleStr = "CASH"
                
            }
             else if self.sugarEnhancementModelArray.dataFeed[indexPath.row].title == "Sugar Hosting" {
                
                urlStr = "https://fayebsg.com/fayebsg_mobile_apps/sugarcrm-dashboard-manager/"
                navigationTitleStr = "Sugar Hosting"
            }
            else if self.sugarEnhancementModelArray.dataFeed[indexPath.row].title == "FayeBSG Sugar Consolidated Activity Report" {
                
                urlStr = "https://fayebsg.com/fayebsg_mobile_apps/sugarcrm-consolidated-activity-report/"
                navigationTitleStr = "CAR"
                
            }
            else if self.sugarEnhancementModelArray.dataFeed[indexPath.row].title == "FayeBSG SugarCRM Dashboard Manager" {
                urlStr = "https://fayebsg.com/fayebsg_mobile_apps/sugarcrm-portal/"
                navigationTitleStr = "DashBoard Manager"
                
            }
            
        }*/
        
        let productDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductsSIWebViewVC") as! ProductsSIWebViewVC
        productDetailsVC.urlStr = urlStr
        productDetailsVC.navigationTitleStr = navigationTitleStr
        
        self.navigationController?.pushViewController(productDetailsVC, animated: true)
        
        
        
    }
   

}
