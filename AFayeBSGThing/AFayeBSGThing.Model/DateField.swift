//
//  DateField.swift
//  MyFitviz
//
//  Created by kdstudio on 29/10/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class DateField: UITextField {
    var date: Date?
    let datePicker = UIDatePicker()
    let formatter = DateFormatter()
    let mode:UIDatePickerMode = .date
    
    override func didMoveToSuperview() {
        datePicker.datePickerMode = mode
        datePicker.minimumDate = Date()
        formatter.dateStyle = .short
        //23-11-2018
        formatter.dateFormat = "dd-MM-yyyy"
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.backgroundColor = UIColor.white
        datePicker.backgroundColor = UIColor.white
        datePicker.tintColor = UIColor(hexString: "333333")
        toolbar.tintColor = UIColor(hexString: "333333")
        
        let done = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneAction))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelAction))
        toolbar.setItems([cancel,space,done], animated: false)
        inputAccessoryView = toolbar
        inputView = datePicker
    }
    
    @objc func cancelAction(_ sender: UIBarButtonItem) {
        endEditing(true)
    }
    
    @objc func doneAction(_ sender: UIBarButtonItem) {
        date = datePicker.date
        text = formatter.string(from: datePicker.date)
        endEditing(true)
    }
}
