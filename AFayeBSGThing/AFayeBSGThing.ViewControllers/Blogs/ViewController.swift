//
//  ViewController.swift
//  WKWebViewLocal
//
//  Created by Gary Newby on 2/17/18.
//  Copyright © 2018 Mellowmuse. All rights reserved.
//

import UIKit
import WebKit
import Social

// Wrap the WKWebView webvvar to allow IB use

class MyWebView : WKWebView {
    required init?(coder: NSCoder) {
        let configuration = WKWebViewConfiguration()
        let controller = WKUserContentController()
        configuration.userContentController = controller;
        super.init(frame: CGRect.zero, configuration: configuration)
    }
}

class ViewController: UIViewController {
    
    var post_idStr : String = ""
    var spinner: UIActivityIndicatorView!
    var blogDetailModelArray  : BlogDetailModel = BlogDetailModel()
    var strBlogHeading = ""
    var strUrlToShare = ""

    @IBOutlet weak var webView: MyWebView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblTitle: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Blog"
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "backArrow") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton

        let customShareButton = UIBarButtonItem(image: UIImage(named: "Share") , style: .plain, target: self, action: #selector(btnShareAction))
        customShareButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.rightBarButtonItem = customShareButton

        webView.navigationDelegate = self
//        webView.uiDelegate = self
        
        // Add addScriptMessageHandler in javascript: window.webkit.messageHandlers.MyObserver.postMessage()
        webView.configuration.userContentController.add(self, name: "MyObserver")
        
        setupSpinner()
        getBlogDetailsAPI(postIDStr: post_idStr)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.title = strBlogHeading
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        // custom actions here
        if(webView.canGoBack) {
            webView.goBack()
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func btnShareAction() {
        
//        let text = ""
//        let image = UIImage(named: "")
        let myWebsite = NSURL(string:strUrlToShare)
        let shareAll = [myWebsite]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)

       /* let alertController = UIAlertController(title: "", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let customShare = UINib(nibName: "CustomShareView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! CustomShareView
        

        customShare.frame = alertController.view.bounds
        customShare.obj = self
        customShare.urlString = strUrlToShare
        alertController.view.addSubview(customShare)
        
//        let somethingAction = UIAlertAction(title: "Something", style: .default, handler: {(alert: UIAlertAction!) in print("something")})
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
        
//        alertController.addAction(somethingAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion:{})*/
        
        

    }
    
    /*func btnFBAction(urlString: String) {
        
        self.dismiss(animated: true, completion: nil)

        let vc = SLComposeViewController(forServiceType:SLServiceTypeFacebook)
        //        vc?.add(UIImage.images)
        vc?.add(URL(string: urlString))
        vc?.setInitialText("Initial text here.")
        self.present(vc!, animated: true, completion: nil)
    }
    
    func btnTwitterShare(urlString: String) {
//        let tweetText = ""
//        let tweetUrl = urlString
//
//        let shareString = "https://twitter.com/intent/tweet?text=\(tweetText)&url=\(tweetUrl)"
//
//        // encode a space to %20 for example
//        let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
//
//        // cast to an url
//        let url = URL(string: escapedShareString)
//
//        // open in safari
//        UIApplication.shared.openURL(url!)
        
        self.dismiss(animated: true, completion: nil)
        
        let vc = SLComposeViewController(forServiceType:SLServiceTypeTwitter)
        //        vc?.add(UIImage.images)
        vc?.add(URL(string: urlString))
        vc?.setInitialText("")
        self.present(vc!, animated: true, completion: nil)

    }
    
    func btnLinkedInShare(urlString: String) {
        
        self.dismiss(animated: true, completion: nil)
        
        let vc = SLComposeViewController(forServiceType:SLServiceTypeLinkedIn)
        //        vc?.add(UIImage.images)
        vc?.add(URL(string: urlString))
        vc?.setInitialText("")
        self.present(vc!, animated: true, completion: nil)
        
    }
    
    func btnLinkedInShare(urlString: String) {
        
        self.dismiss(animated: true, completion: nil)
        
        let vc = SLComposeViewController(forServiceType:SLServiceTypeLinkedIn)
        //        vc?.add(UIImage.images)
        vc?.add(URL(string: urlString))
        vc?.setInitialText("")
        self.present(vc!, animated: true, completion: nil)
        
    }*/


    
    @IBAction func callJavascriptTapped(_ sender: UIButton) {
        let script = "testJS()"
        webView.evaluateJavaScript(script) { (result: Any?, error: Error?) in
            if let error = error {
                print("evaluateJavaScript error: \(error.localizedDescription)")
            } else {
                print("evaluateJavaScript result: \(result ?? "")")
            }
        }
    }
    
    func setupSpinner(){
        spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height:40))
        spinner.color = UIColor.darkGray
        self.spinner.center = CGPoint(x:UIScreen.main.bounds.size.width / 2, y:UIScreen.main.bounds.size.height / 2)
        self.view.addSubview(spinner)
        spinner.hidesWhenStopped = true
    }
    
    func getBlogDetailsAPI(postIDStr : String) {
        
        var blogDetailsDict: [String: Any] = [:]
        
        blogDetailsDict["post_id"] = postIDStr
        
        GenericMethods.showLoading()
        
        WebAPIHelper.postMethod(params: blogDetailsDict , methodName: "/ApiBlog.php?method=blog_detail", success: {
            (response)->Void in
            
            print("getBlogsCategoryWiseAPI Response \(response!)")
            
            if !(response is NSNull)
            {
                
                DispatchQueue.main.async {
                    GenericMethods.hideLoading()
                }
                self.blogDetailModelArray = Parser.parseBlogDetailModel(apiResponse: response!)
                
                if self.blogDetailModelArray.message == "Success"{
                    
                    DispatchQueue.main.async {
                        print("HTML CODE is \(self.blogDetailModelArray.dataFeed[0].content)")
                        self.spinner.startAnimating()
                        self.lblDate.text = self.blogDetailModelArray.dataFeed[0].date
                        self.lblTitle.text = self.blogDetailModelArray.dataFeed[0].title
                        self.lblCategory.text = self.blogDetailModelArray.dataFeed[0].category

//                        self.webView.loadHTMLString(self.blogDetailModelArray.dataFeed[0].content, baseURL: nil)
                        // Choose to load a file or a string
                        let loadFile = false
                        
                        if let filePath = Bundle.main.path(forResource:"index", ofType:"html", inDirectory: "Web_Assets") {
                            if (loadFile) {
                                // load file
                                let filePathURL = URL.init(fileURLWithPath: filePath)
                                let fileDirectoryURL = filePathURL.deletingLastPathComponent()
                                self.webView.loadFileURL(filePathURL, allowingReadAccessTo: fileDirectoryURL)
                            } else {
                                do {
                                    // load html string - baseURL needs to be set for local files to load correctly
//                                    let html = try String(contentsOfFile: filePath, encoding: .utf8)
                                    let html = "<html><head><meta name=\"viewport\" content=\"initial-scale=1.0\" /><link href=\"css/app.css\" rel=\"stylesheet\" type=\"text/css\"></head><body>" + self.blogDetailModelArray.dataFeed[0].content + "<a href=\"index_contactUs_Blog.html\"><button type=\"button\" style=\"border: 2px solid #035db8;background-color: #035db8;color: #ffffff; padding: 10px 20px;width:100%; font-size:16px;\">Contact Us To Learn More</button></a></body></html>"
                                    self.webView.loadHTMLString(html, baseURL: Bundle.main.resourceURL?.appendingPathComponent("Web_Assets"))
                                } catch {
                                    print("Error loading html")
                                }
                            }
                        }

                    }
                }
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.hideLoading()
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                }
            }
        }, Failure: {
            (error)->Void in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
                GenericMethods.showAlert(alertMessage: ErrorMessages.SigninIncorrectDetailsAlert)
            }
        })
    }
}

extension ViewController : WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        // Callback from javascript: window.webkit.messageHandlers.MyObserver.postMessage(message)
        let text = message.body as! String;
        let alertController = UIAlertController(title: "Javascript said:", message: text, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            print("OK")
        }
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
}

extension ViewController : WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("didFinish navigation:");
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        //this is a 'new window action' (aka target="_blank") > open this URL externally. If we´re doing nothing here, WKWebView will also just do nothing. Maybe this will change in a later stage of the iOS 8 Beta
        
        if (navigationAction.request.url?.absoluteString.contains(find: "forms/userSubmit.jsp"))! {
            self.setupSpinner()
            self.spinner.startAnimating()
            decisionHandler(.allow)
            
        }
        else if (navigationAction.request.url?.absoluteString.contains(find: "fayebsg.com/thanks"))! {
            self.spinner.stopAnimating()
            
                if(webView.canGoBack) {
                    webView.goBack()
                }
                else {
                    self.navigationController?.popViewController(animated: true)
                }
            
            decisionHandler(.cancel)
            
        }
        else if navigationAction.navigationType == WKNavigationType.linkActivated {
            print("here link Activated!!!")
            
            let url = navigationAction.request.url
            
            let urlStr = url!.absoluteString
            
            if urlStr.contains(find: "index_contactUs_Blog.html") {
                self.spinner.startAnimating()
                webView.load(navigationAction.request)
            }
            else {
                UIApplication.shared.open(URL(string : urlStr)!, options: [:], completionHandler: { (status) in
                    
                })
            }
            
            decisionHandler(WKNavigationActionPolicy.cancel)
        }
        else{
            decisionHandler(WKNavigationActionPolicy.allow)
        }
    }
}

//extension ViewController : WKUIDelegate {
//
//    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
//
////        if navigationAction.targetFrame == nil {
////            webView.load(navigationAction.request)
//            let url = navigationAction.request.url
//            let shared = UIApplication.shared
//
//        _ = url!.absoluteString
//
//            if shared.canOpenURL(url!) {
//                shared.openURL(url!)
//            }
////        }
//
//        return nil
//    }
//
////    func webView(_ webView: WKWebView!, createWebViewWith configuration: WKWebViewConfiguration!, for navigationAction: WKNavigationAction!, windowFeatures: WKWindowFeatures!) -> WKWebView! {
////        if navigationAction.targetFrame == nil {
////            webView.load(navigationAction.request)
////        }
////        return nil
////    }
//}


