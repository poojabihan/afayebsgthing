//
//  AddAppointments.swift
//  FayeBSG
//
//  Created by Apple on 03/11/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class AddAppointments: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtTime: TimeField!
    @IBOutlet weak var txtDate: DateField!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnSave: UIButton!

    var objParent = AppointmentsList()
    var textField: UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(PageViewController.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PageViewController.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        self.hideKeyboardWhenTappedAround()
        btnSave.layer.cornerRadius = 20

        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 40))
        txtDate.leftView = paddingView
        txtDate.leftViewMode = .always

        let paddingView2: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 40))
        txtTime.leftView = paddingView2
        txtTime.leftViewMode = .always

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnbackAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func btnSaveAction(_ sender: Any) {
        
        if (txtDate.text?.isEmpty)! {
            
            let alert = UIAlertController(title: "", message: "Please select Date.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }
        else if (txtTime.text?.isEmpty)! {
            
            let alert = UIAlertController(title: "", message: "Please select Time.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }
        else if (txtDescription.text?.isEmpty)! {
            
            let alert = UIAlertController(title: "", message: "Please enter Description.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }
        /*else if (txtDescription.text? == "Enter Description") {
            
            let alert = UIAlertController(title: nil, message: "Please enter Description.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }*/
        else{
            
            callWebServiceToBookAppointment()
        }
    }
    
    func callWebServiceToBookAppointment(){
        
        /*{
         "userId":"8",
         "date":"23-11-2018",
         "time":"6:00",
         "description":"test"
         
         }*/
            
            GenericMethods.showLoading()
            let param =
                ["userId": UserDefaults.standard.value(forKey: "id") ?? "",
                 "date":txtDate.text ?? "" ,
                 "time":txtTime.text ?? "",
                 "description":txtDescription.text ?? "",
                 ] as [String : Any]
            
            print(param)
            
            WebAPIHelper.postMethod(params: param, methodName: "/ApiHome.php?method=bookAppointment", success: { (response) in
                
                print("Response \(response!)")
                DispatchQueue.main.async{
                    GenericMethods.hideLoading()
                }
                
                if !(response is NSNull) {
                    
                    self.btnbackAction(self)
                    self.objParent.callWebServiceTogetAppointments()

//                    let alert = UIAlertController(title: nil, message: response?["message"] as? String, preferredStyle: .alert)
//
//                    alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (ok) in
//                        self.btnbackAction(self)
//                        self.objParent.callWebServiceTogetAppointments()
//                    }))
//
//                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
//                    DispatchQueue.main.async {
//
//                        self.present(alert, animated: true)
//
//                    }
                    /* UserDefaults.standard.set("Yes", forKey: "LoggedIn") //Bool
                     UserDefaults.standard.set(txtEmail, forKey: "Email") //Bool
                     
                     DispatchQueue.main.async {
                     self.loginView.isHidden = true
                     }*/
                }
                else
                {
                    DispatchQueue.main.async {
                        GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                        return
                    }
                }
                
            }) { (error) in
                print("Error \(error)")
                DispatchQueue.main.async {
                    GenericMethods.hideLoading()
                }
            }
            
        
    }
    
    //MARK: - UITextFiled keyboard hide unhide methods
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    
    //MARK: - UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }

}
