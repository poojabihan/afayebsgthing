//
//  Validation.swift
//  Express1099
//
//  Created by Sanjeev T on 02/01/17.
//  Copyright © 2017 SPAN Technology. All rights reserved.
//

import UIKit

class Validation: NSObject {
    
    
    //Checking if string is empty
    class func isStringEmpty(_ str: String) -> Bool {
        if str.isEmpty || (str.count ) == 0 || ((str.trimmingCharacters(in: CharacterSet.whitespaces).count ) == 0) {
            return true
        }
        return false
    }

   class func canOpenURL(string: String?) -> Bool {
        guard let urlString = string else {return false}
        guard let url = NSURL(string: urlString) else {return false}
        if !UIApplication.shared.canOpenURL(url as URL) {return false}
        
        //
        let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
        return predicate.evaluate(with: string)
    }

    class func verifyUrl (urlString: String?) -> Bool
    {
        //Check for nil
        if let urlString = urlString
        {
            // create NSURL instance
            if let url = NSURL(string: urlString)
            {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    //Remove NULL from string
    class func removeNullFromString (_ string: String) -> String
    {
        if string.isEmpty || string == "<null>"
        {
            return ""
        }
        else
        {
            return string
        }
    }
    
    class func removeNullFromString (_ string: String, replaceWithString: String) -> String
    {
        if string.isEmpty || string == "<null>"
        {
            return replaceWithString
        }
        else
        {
            return string
        }
    }

    //MARK:- Check Empty for a textfield
    class func isEmptyTextField (_ textField: UITextField) -> Bool
    {
        if (textField.text == nil || textField.text == "")
        {
            
            return true
        }
        return false
    }
    
    class func isEmptyTextView (_ textView: UITextView) -> Bool
    {
        if (textView.text == nil || textView.text == "")
        {
            
            return true
        }
        return false
    }
    
    //Mark:- Email Regex Check
    class func isValidEmail(_ email:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        print(emailTest.evaluate(with: email))
        return emailTest.evaluate(with: email)
    }
    
    class func isValidCVV(_ cvv:String) -> Bool
    {
        let string = cvv as NSString
        if string.length != 3
        {
            GenericMethods.showAlert(alertMessage: "CVV should be 3 letters")
            return false
        }
        return true
    }
    
    
    class func isValidZipCode(_ zipcode:String) -> Bool
    {
        let string = zipcode as NSString
        var zipcodeExpression: String = ""
        zipcodeExpression = "^\\d{5}(-\\d{4})?$"
        do
        {
            let regex = try NSRegularExpression(pattern: zipcodeExpression, options: [])
            let match: NSTextCheckingResult? = regex.firstMatch(in: string as String, options: [], range: NSRange(location: 0, length: string.length))
            if match != nil
            {
                return true
            }
            else
            {
                if !(string.length == 5) || (string.length == 10)
                {
                    return false
                }
                else
                {
                    return true
                }
                
            }
        }
        catch
        {
        }
         return false
    }
    
    class func isValidPostalCode(_ postal:String) -> Bool
    {
        let string = postal as NSString
        var zipcodeExpression: String = ""
        zipcodeExpression = "^[ABCEGHJKLMNPRSTVXY]{1}\\d{1}[A-Z]{1} *\\d{1}[A-Z]{1}\\d{1}$"
        do
        {
            let regex = try NSRegularExpression(pattern: zipcodeExpression, options: [])
            let match: NSTextCheckingResult? = regex.firstMatch(in: string as String, options: [], range: NSRange(location: 0, length: string.length))
            if match != nil
            {
                return true
            }

        }
        catch
        {
        }
        return false
    }
    
    class func isValidPhoneNumber(_ number:String) -> Bool
    {
        let string = number as NSString
        if string.length != 14
        {
            return false
        }
        return true
    }
    
    class func isPasswordMinimumSix(_ number:String) -> Bool
    {
        let string = number as NSString
        if string.length < 6
        {
            return false
        }
        return true
    }

    class func checkStatus(response: AnyObject) -> Bool {
        if let statusStr: String = response["Status"] as? String
        {
            if statusStr == "Success"
            {
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            return false
        }
    }

    //Some valid Url's : http://iosrevisited.blogspot.com, http://adeepdrive.com.

    class func isValidUrl(enteredVideoUrl: String, enteredVideoUrlType : String) -> Bool
     {
        var returnBool : Bool = false

        if (enteredVideoUrlType == "Yahoo")
        {
            let Url = "http://video.yahoo.com/watch?";
            let Url1 = "http://comedy.video.yahoo.com";
            let Url2 = "http://animalvideos.yahoo.com";
            let Url3 = "http://video.yahoo.com/watchmojo";
            let Url4 = "http://video.yahoo.com/momentsofmotherhood";
            let Url5 = "http://video.yahoo.com/tlc";
            let Url6 = "https://video.yahoo.com/watch?";
            let Url7 = "https://comedy.video.yahoo.com";
            let Url8 = "https://animalvideos.yahoo.com";
            let Url9 = "https://video.yahoo.com/watchmojo";
            let Url10 = "https://video.yahoo.com/momentsofmotherhood";
            let Url11 = "https://video.yahoo.com/tlc";
            let Url12 = "https://news.yahoo.com/video";

            var UrlArray : [String] = [Url,Url1,Url2,Url3,Url4,Url5,Url6,Url7,Url8,Url9,Url10,Url11,Url12]

            for i in 0..<UrlArray.count
            {
                if enteredVideoUrl.range(of:UrlArray[i]) != nil
                {
                    returnBool = true
                }
                else
                {
                    returnBool = false
                }

            }
        }
        else if (enteredVideoUrlType == "Vimeo")
        {
            let Url = "http://vimeo.com/";
            let Url1 = "https://vimeo.com/";

             var UrlArray : [String] = [Url,Url1]

            for i in 0..<UrlArray.count
            {
                if enteredVideoUrl.range(of:UrlArray[i]) != nil
                {
                    returnBool = true
                }
                else
                {
                    returnBool = false
                }

            }
        }
        else if (enteredVideoUrlType == "YouTube")
        {
            let Url = "http://www.youtube.com/watch?"
            let Url1 = "https://www.youtube.com/watch?"
            let Url2 = "https://youtu.be/"
            let Url3 = "http://youtu.be/NLqAF9hrVbY"
            let Url4 = "http://www.youtube.com/watch?feature=player_embedded&v=DJjDrajmbIQ"
            let Url5 = "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
            let Url6 = "http://www.youtube.com/embed/NLqAF9hrVbY"
            let Url7 = "https://www.youtube.com/embed/NLqAF9hrVbY"
            let Url8 = "http://www.youtube.com/watch?v=NLqAF9hrVbY"
            let Url9 = "http://www.youtube.com/v/dQw4w9WgXcQ"
            let Url10 = "http://youtube.com/v/NLqAF9hrVbY?fs=1&hl=en_US"
            let Url11 = "http://www.youtube.com/v/NLqAF9hrVbY?fs=1&hl=en_US"
            let Url12 = "http://youtube.com/watch?v=NLqAF9hrVbY"
            let Url13 = "http://www.youtube.com/user/Scobleizer#p/u/1/1p3vcRhsYGo"
            let Url14 = "http://www.youtube.com/ytscreeningroom?v=NRHVzbJVx8I"
            let Url15 = "http://www.youtube.com/sandalsResorts#p/c/54B8C800269D7C1B/2/PPS-8DMrAn4"
            let Url16 = "http://gdata.youtube.com/feeds/api/videos/NLqAF9hrVbY"
            let Url17 = "http://www.youtube.com/watch?v=spDj54kf-vY&feature=g-vrec"

            var UrlArray : [String] = [Url,Url1,Url2,Url3,Url4,Url5,Url6,Url7,Url8,Url9,Url10,Url11,Url12,Url13,Url14,Url15,Url16,Url17]

            for i in 0..<UrlArray.count
            {
                if enteredVideoUrl.range(of:UrlArray[i]) != nil
                {
                    returnBool = true
                }
                else
                {
                    returnBool = false
                }

            }
        }
        else if (enteredVideoUrlType == "Facebook")
        {
            let Url = "http://www.facebook.com/video";
            let Url1 = "https://www.facebook.com/video";
            let Url2 = "http://www.facebook.com/photo";
            let Url3 = "https://www.facebook.com/photo";

            var UrlArray : [String] = [Url,Url1,Url2,Url3]

            for i in 0..<UrlArray.count
            {
                if enteredVideoUrl.range(of:UrlArray[i]) != nil
                {
                    returnBool = true
                }
                else
                {
                    returnBool = false
                }

            }
        }

        return returnBool
    }

    class func isValidDeviceTime(currentServerTime: String) -> Bool
    {
        print(currentServerTime)
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss zzz"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let currentDeviceTime = formatter.string(from: Date())
        print(currentDeviceTime)
        
        let currentDeviceTime_Date = formatter.date(from: currentDeviceTime)!
        let currentServerTime_Date = formatter.date(from: currentServerTime)!
        
        print(currentDeviceTime_Date)
        print(currentServerTime_Date)
        
        let calendar = Calendar.current
        let currentServerTime_Date_minMargin = calendar.date(byAdding: .minute, value: -5, to: currentServerTime_Date)
        let currentServerTime_Date_maxMargin = calendar.date(byAdding: .minute, value: 5, to: currentServerTime_Date)
        
        print(currentServerTime_Date_minMargin!)
        print(currentServerTime_Date_maxMargin!)
        
        let isGreaterThanMinMargin: Bool = currentDeviceTime_Date > currentServerTime_Date_minMargin!
        let isLesserThanMaxMargin: Bool = currentDeviceTime_Date < currentServerTime_Date_maxMargin!
        
        if (isGreaterThanMinMargin && isLesserThanMaxMargin)
        {
            print("Valid Device Time")
            
            return true
        }
        else
        {
            print("Invalid Device Time")
            
            return false
        }
    }

}
