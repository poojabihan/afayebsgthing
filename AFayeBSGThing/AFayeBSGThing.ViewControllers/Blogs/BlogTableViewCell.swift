//
//  BlogTableViewCell.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/1/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class BlogTableViewCell: UITableViewCell {

    @IBOutlet weak var blogBGView: UIView!
    
    @IBOutlet weak var blogImageView: UIImageView!
    
    @IBOutlet weak var blogHeaderName: UILabel!
    
    @IBOutlet weak var blogSubTitleNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setDesignStyles()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK:- CSS Implementation
    fileprivate func setDesignStyles()
    {
       
        self.blogBGView.backgroundColor = UIColor.white
        self.blogBGView.layer.cornerRadius = 5.0
        self.blogBGView.layer.borderColor = UIColor.lightGray.cgColor
        self.blogBGView.layer.borderWidth = 0.2
        self.blogBGView.layer.shadowColor = UIColor.lightGray.cgColor
        self.blogBGView.layer.shadowOpacity = 1.0
        self.blogBGView.layer.shadowRadius = 5.0
        self.blogBGView.layer.shadowOffset = CGSize(width: 0, height: 2)
        
        
    }
}
