//
//  SageSIViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/8/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import youtube_ios_player_helper
class SageSIViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var youtubeView: YTPlayerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
self.navigationItem.title = "Sage"
        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
        
        youtubeView.load(withVideoId: "ZhT5PiHXBX8")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: ScreenUtils.CurrentDeviceWidth, height: contactView.frame.origin.y + contactView.frame.size.height + 10)
    }
    
    
    @IBAction func contactusBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Contact")
    }
    
    @IBAction func specSheetBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Specsheet")
    }
    
    @IBAction func editionMatrixBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "EditionMatrix")
    }
    
    
    @IBAction func supportBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Support")
    }
    
    @IBAction func professionalEditionBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "ProfessionalEditionBuyNow")
    }
    
    @IBAction func enterpriseActionBtnAction(_ sender: Any) {
         pushToQuickBooksDetails(type: "EnterpriseEditionAction")
    }
    
    @IBAction func phoneNumberBtnAction(_ sender: Any) {
        guard let number = URL(string: "tel://" + "8182804820") else { return }
        UIApplication.shared.open(number)
    }
    
    
    func pushToQuickBooksDetails(type : String) {
        let sageSIWebViewVC = self.storyboard?.instantiateViewController(withIdentifier: "SageSIWebViewVC") as! SageSIWebViewVC
        sageSIWebViewVC.typeOfStr = type
        self.navigationController?.pushViewController(sageSIWebViewVC, animated: true)
    }

}
