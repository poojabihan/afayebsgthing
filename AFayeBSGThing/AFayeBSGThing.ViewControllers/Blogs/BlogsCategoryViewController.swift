//
//  BlogsCategoryViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/1/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class BlogsCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var blogsTableView: UITableView!
    var category_idStr : String = ""
    var categoryNameStr : String = ""
    var blogModelArray  : BlogModel = BlogModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)

        self.navigationItem.title = categoryNameStr + " Blog"
        blogsTableView.register(UINib(nibName:"BlogTableViewCell" , bundle: nil), forCellReuseIdentifier: "BlogTableViewCell")
        getBlogsCategoryWiseAPI(category_id: category_idStr)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    

    func getBlogsCategoryWiseAPI(category_id : String) {
        var blogCategoryWiseDict: [String: Any] = [:]
        
        blogCategoryWiseDict["category_id"] = category_id
        
        GenericMethods.showLoading()
        
        WebAPIHelper.postMethod(params: blogCategoryWiseDict , methodName: "/ApiBlog.php?method=blog_category", success: {
            (response)->Void in
            
            print("getBlogsCategoryWiseAPI Response \(response!)")
            
            if !(response is NSNull)
            {
                
                self.blogModelArray = Parser.parseBlogModel(apiResponse: response!)
                
                
                if self.blogModelArray.message == "Success"{
                    
                    
                    
                    
                }
                DispatchQueue.main.async{
                    GenericMethods.hideLoading()
                    self.blogsTableView.reloadData()
                    
                }
                
              
                
                
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.hideLoading()
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                }
            }
        }, Failure: {
            (error)->Void in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
//                GenericMethods.showAlert(alertMessage: ErrorMessages.SigninIncorrectDetailsAlert)
            }
        })
    }
    
    //MARK:- Tableview Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.blogModelArray.dataFeed.count
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:BlogTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BlogTableViewCell", for: indexPath) as! BlogTableViewCell
        cell.blogSubTitleNameLbl.numberOfLines = 0
        
        cell.blogImageView.sd_setImage(with: URL(string: self.blogModelArray.dataFeed[indexPath.row].image), placeholderImage: #imageLiteral(resourceName: "placeholder_Image"))
        
        cell.blogHeaderName.text = self.blogModelArray.dataFeed[indexPath.row].category
        cell.blogSubTitleNameLbl.text = self.blogModelArray.dataFeed[indexPath.row].title
        
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let blogDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "BlogDetailViewController") as! BlogDetailViewController
//        blogDetailVC.post_idStr = self.blogModelArray.dataFeed[indexPath.row].post_id
//
//        self.navigationController?.pushViewController(blogDetailVC, animated: true)
        
        let blogDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        blogDetailVC.post_idStr = self.blogModelArray.dataFeed[indexPath.row].post_id
        blogDetailVC.strUrlToShare = self.blogModelArray.dataFeed[indexPath.row].link
//        blogDetailVC.strBlogHeading = self.blogModelArray.dataFeed[indexPath.row].title

        self.navigationController?.pushViewController(blogDetailVC, animated: true)

    }

}
