//
//  ServicesModel.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 4/28/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import ObjectMapper

class ServicesModel: Mappable {

    var dataFeed = [servicesData]()
    var status : Bool = false
    var message : String = ""
    var service_name : String = ""
    
    
    required init?(map: Map)
    {
        
    }
    init()
    {
        
        
    }
    func mapping(map: Map)
    {
        
        status <- map["status"]
        message <- map["message"]
        dataFeed <- map["result"]
        service_name <- map["service_name"]
        
    }
    
}

class servicesData: Mappable
{
    var image :String = ""
    var text :String = ""
    var title :String    = ""
    
    
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    func mapping(map: Map)
    {
        image                   <- map["image"]
        text                    <- map["text"]
        title                <- map["title"]
        
        
        
    }
    
}

