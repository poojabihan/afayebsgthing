//
//  PageViewController.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 4/28/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import CarbonKit

class PageViewController: UIViewController, CarbonTabSwipeNavigationDelegate, UITextFieldDelegate {
    
    //MARK:- Variable declaration
    var tabSwipe : CarbonTabSwipeNavigation!
    var textField: UITextField?
    
    @IBOutlet weak var pagerView: UIView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var tblView: UITableView!

    //    var txtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeTabbarIndex(notification:)), name: Notification.Name("ChangeIndex"), object: nil)
        //            let logo = UIImage(named: "Navigation")
        //        //self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 500)
        //
        //        if (logo != nil) {
        //           // let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        //            //imageView.contentMode = .scaleAspectFit
        //            //let image = UIImage(named: "Navigation")
        //
        //            self.navigationItem.titleView = UIImageView.init(image: logo)
        //        }
        //        else{
        //            self.navigationItem.titleView = nil
        //            self.navigationItem.title = "A FayeBSG Thing"
        //        }
        
        btnSubmit.layer.cornerRadius = 20
        
        setUpperTabBars()
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(PageViewController.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PageViewController.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)

        self.hideKeyboardWhenTappedAround()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.object(forKey: "LoggedIn") == nil {
            
            loginView.isHidden = false
            // txtEmail.addTarget(self, action: #selector(textDidChangeInLoginAlert), for: .editingChanged)
            // btnSubmit.isEnabled = false
            //            let alert = UIAlertController(title: "Enter Email:", message: nil, preferredStyle: .alert)
            //
            //            alert.addTextField {
            //                $0.placeholder = ""
            //                $0.addTarget(alert, action: #selector(alert.textDidChangeInLoginAlert), for: .editingChanged)
            //            }
            //
            //
            //            let loginAction = UIAlertAction(title: "OK", style: .default) { [unowned self] _ in
            //                guard let email = alert.textFields?[0].text
            //                    else { return } // Should never happen
            //
            //                print("email",email)
            //                self.webServiceCallingEmailSave(txtEmail: email)
            //
            //                // Perform login action
            //            }
            //
            //
            //            loginAction.isEnabled = false
            //            alert.addAction(loginAction)
            //            present(alert, animated: true)
            //            openEmailAlert()
        }
        else {
            loginView.isHidden = true
        }
        //        if UserDefaults.standard.object(forKey: "LoggedIn") != nil {
        //            let strValue = UserDefaults.standard.object(forKey: "LoggedIn") as! String
        //
        //            if  strValue == "Yes" {
        //                openEmailAlert()
        //            }
        //        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /* @IBAction func btnSubmitAction(){
     self.view.endEditing(true)
     
     self.webServiceCallingEmailSave(txtEmail: txtEmail.text!)
     }*/
    
    @objc func changeTabbarIndex(notification: NSNotification) {
        var dict = notification.object as? Dictionary<String, Any>
        
        print(notification.object ?? "default")
        let index = dict!["index"] as! Int
        tabSwipe.setCurrentTabIndex(UInt(index), withAnimation: true)
    }
    
    fileprivate func setUpperTabBars()
    {
        let homeIcon = UIImage(named: "home")
        
        //        tabSwipe = CarbonTabSwipeNavigation(items: [homeIcon ?? "HOME", "PRODUCTS", "SERVICES", "SUGARCRM", "BLOGS"], delegate: self)
        tabSwipe = CarbonTabSwipeNavigation(items: [homeIcon ?? "HOME", "BLOGS", "PRODUCTS", "SERVICES", "SUGARCRM", "CONTACT US"], delegate: self)
        
        //tabSwipe.toolbar.barTintColor = Colors.THEMECOLOR
        tabSwipe.toolbar.barTintColor = Colors.THEMECOLOR
        tabSwipe.setNormalColor(UIColor.white)
        tabSwipe.setSelectedColor(UIColor(red: 33/255, green: 95/255, blue: 172/255, alpha: 1.0))
        tabSwipe.setTabExtraWidth(15)
        //tabSwipe.insert(intoRootViewController: self)
        tabSwipe.insert(intoRootViewController: self, andTargetView: pagerView)
        
        //tabSwipe.enabl = false
        
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        guard let storyboard = storyboard else { return UIViewController() }
        if index == 0 {
            return storyboard.instantiateViewController(withIdentifier: "HomeViewController")
        }
        else if index == 1 {
            return storyboard.instantiateViewController(withIdentifier: "BlogsViewController")
        }
        else if index == 2{
            return storyboard.instantiateViewController(withIdentifier: "ProductViewController")
        }
        else if index == 3 {
            return storyboard.instantiateViewController(withIdentifier: "ServicesViewController")
        }
        else if index == 4{
            return storyboard.instantiateViewController(withIdentifier: "SugarCRMViewController")
        }
        else {
            return storyboard.instantiateViewController(withIdentifier: "ContactUsViewController")
        }
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("ChangeIndex"), object: nil)
    }
    
    /* func openEmailAlert(){
     let alert = UIAlertController(title: "Enter Email:", message: nil, preferredStyle: .alert)
     
     alert.addTextField {
     $0.placeholder = ""
     $0.addTarget(alert, action: #selector(alert.textDidChangeInLoginAlert), for: .editingChanged)
     }
     
     
     let loginAction = UIAlertAction(title: "OK", style: .default) { [unowned self] _ in
     guard let email = alert.textFields?[0].text
     else { return } // Should never happen
     
     print("email",email)
     //self.webServiceCallingEmailSave()
     
     // Perform login action
     }
     
     
     loginAction.isEnabled = false
     alert.addAction(loginAction)
     present(alert, animated: true)
     
     }*/
    
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        if (txtEmail.text?.isEmpty)! {
            
            let alert = UIAlertController(title: nil, message: "Please enter Email.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }
        else if (txtEmail.text!.isValidEmail() == false){
            
            let alert = UIAlertController(title: nil, message: "Please enter valid Email.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }
        else if (txtPassword.text?.isEmpty)! {
            
            let alert = UIAlertController(title: nil, message: "Please enter Password.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }
        else if(checkForSpace(strCheckString: txtPassword.text!) ==  true){
            
            let alert = UIAlertController(title: nil, message: "White space not allowed in the password field.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
        }
        else{
            webServiceCallingLogin()
        }
        
        
    }
    
    @IBAction func btnRegisterAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegistrationView") as! RegistrationView
        objVC.modalPresentationStyle = .overCurrentContext
        self.present(objVC, animated: false, completion: nil)
        
    }
    
    @IBAction func btnForgotPasswordAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        objVC.modalPresentationStyle = .overCurrentContext
        self.present(objVC, animated: false, completion: nil)
        
    }
    
    func checkForSpace(strCheckString : String) -> Bool {
        
        let whiteSpace = " "
        if strCheckString.contains(whiteSpace) {
            print(strCheckString)
            print ("has whitespace")
            return true
        } else {
            print(strCheckString)
            print("no whitespace")
            return false
        }
        
    }
    
    
    func webServiceCallingLogin()
    {
        
        GenericMethods.showLoading()
        /*{
         "email":"pooja1@chetaru.com",
         "password":"123456"
         }
         */
        let param =
            ["email":txtEmail.text ?? "",
             "password":txtPassword.text ?? ""
                ] as [String : Any]
        
        print(param)
        
        WebAPIHelper.postMethod(params: param, methodName: "/ApiHome.php?method=login", success: { (response) in
            
            print("Response \(response!)")
            DispatchQueue.main.async{
                GenericMethods.hideLoading()
            }
            
            if !(response is NSNull) {
                
                if response!["status"] as? Bool == true{
                    let alert = UIAlertController(title: nil, message: response?["message"] as? String , preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    
                    UserDefaults.standard.set("Yes", forKey: "LoggedIn") //Bool
                    UserDefaults.standard.set(self.txtEmail.text, forKey: "Email") //Bool
                    print(response?.value(forKeyPath: "result.id") as! String)
                    UserDefaults.standard.set(response?.value(forKeyPath: "result.id") as! String, forKey: "id")
                    
                    
                    DispatchQueue.main.async {
                        
                       // self.present(alert, animated: true)
                        self.loginView.isHidden = true
                        
                    }
                }
                    
                else{
                    let alert = UIAlertController(title: nil, message: response?["message"] as? String , preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    DispatchQueue.main.async {
                        self.present(alert, animated: true)
                    
                        self.txtEmail.text = ""
                        self.txtPassword.text = ""
                    }
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
                    return
                }
            }
            
        }) { (error) in
            print("Error \(error)")
            DispatchQueue.main.async {
                GenericMethods.hideLoading()
            }
        }
    }
    
    //MARK: - UITextFiled keyboard hide unhide methods
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    
    //MARK: - UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    /*GenericMethods.showLoading()
     WebAPIHelper.postMethod(params: ["email" : txtEmail], methodName: "/ApiHome.php?method=saveEmail", success: { (response) in
     
     print("Response \(response!)")
     DispatchQueue.main.async{
     GenericMethods.hideLoading()
     }
     
     if !(response is NSNull) {
     UserDefaults.standard.set("Yes", forKey: "LoggedIn") //Bool
     UserDefaults.standard.set(txtEmail, forKey: "Email") //Bool
     
     DispatchQueue.main.async {
     self.loginView.isHidden = true
     }
     }
     else
     {
     DispatchQueue.main.async {
     GenericMethods.showAlert(alertMessage: ErrorMessages.SomeErrorOccurred)
     return
     }
     }
     
     }) { (error) in
     print("Error \(error)")
     DispatchQueue.main.async {
     GenericMethods.hideLoading()
     }
     }*/
    
    
    /*  @objc func textDidChangeInLoginAlert() {
     btnSubmit.isEnabled = isValidEmail(txtEmail.text!) //&& isValidPassword(password)
     }
     
     func isValidEmail(_ email: String) -> Bool {
     return email.count > 0 && NSPredicate(format: "self matches %@", "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,64}").evaluate(with: email)
     }
     
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
     self.view.endEditing(true)
     return false
     }*/
    
}

/*extension UIAlertController {
 
 func isValidEmail(_ email: String) -> Bool {
 return email.count > 0 && NSPredicate(format: "self matches %@", "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,64}").evaluate(with: email)
 }
 
 func isValidPassword(_ password: String) -> Bool {
 return password.count > 4 && password.rangeOfCharacter(from: .whitespacesAndNewlines) == nil
 }
 
 @objc func textDidChangeInLoginAlert() {
 if let email = textFields?[0].text,
 //            let password = textFields?[1].text,
 let action = actions.last {
 action.isEnabled = isValidEmail(email) //&& isValidPassword(password)
 }
 }
 }
 */

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
