//
//  BlogDetailModel.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/1/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import ObjectMapper

class BlogDetailModel: Mappable {

    var dataFeed = [blogDetailData]()
    var status : Bool = false
    var message : String = ""
    var service_name : String = ""
    
    
    required init?(map: Map)
    {
        
    }
    init()
    {
        
        
    }
    func mapping(map: Map)
    {
        
        status <- map["status"]
        message <- map["message"]
        dataFeed <- map["result"]
        service_name <- map["service_name"]
        
    }
}

class blogDetailData: Mappable
{
    var image :String = ""
    var title :String = ""
    var content :String = ""
    var date :String = ""
    var category :String = ""
    
    
    
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    func mapping(map: Map)
    {
        image                   <- map["image"]
        title                    <- map["title"]
        date                    <- map["date"]
        content                    <- map["content"]
        category                    <- map["category"]
    }
        
        
        
}
