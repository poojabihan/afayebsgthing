//
//  ContactUsViewController.swift
//  AFayeBSGThing
//
//  Created by Apple on 20/06/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import WebKit

//class contactWebView : WKWebView {
//    required init?(coder: NSCoder) {
//        let configuration = WKWebViewConfiguration()
//        let controller = WKUserContentController()
//        controller.addScriptMessageHandler(
//            self,
//            name: "callbackHandler"
//        )
//
//        configuration.userContentController = controller;
//        super.init(frame: CGRect.zero, configuration: configuration)
//    }
//}

class ContactUsViewController:  UIViewController, WKUIDelegate {
    
//    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var webView: WKWebView!
//    @IBOutlet weak var webView: contactWebView!
    var showNav = false

var spinner: UIActivityIndicatorView!
override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
//    self.navigationItem.title = "Blog Details"
//    self.automaticallyAdjustsScrollViewInsets = false
//    webView.navigationDelegate = self;
    

    let customBackButton = UIBarButtonItem(image: UIImage(named: "backArrow") , style: .plain, target: self, action: #selector(backAction(sender:)))
    customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
    navigationItem.leftBarButtonItem = customBackButton
    
    self.title = "Contact us"
    setupSpinner()
    

    GenericMethods.showLoading()
    let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
    let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
    let wkUController = WKUserContentController()
    wkUController.addUserScript(userScript)
    let wkWebConfig = WKWebViewConfiguration()
    wkWebConfig.userContentController = wkUController
    webView = WKWebView(frame: self.view.bounds, configuration: wkWebConfig)

    
    refreshUrl()
    
    webView.uiDelegate = self
    webView.navigationDelegate = self
    webView.configuration.userContentController.add(self, name: "MyObserver")
    view = webView

}
    
    func refreshUrl() {
        let htmlPath = Bundle.main.path(forResource:"index", ofType:"html", inDirectory: "Web_Assets")//Bundle.main.path(forResource: "index", ofType: "html")
        let folderPath = Bundle.main.bundlePath

        let baseUrl = URL(fileURLWithPath: folderPath, isDirectory: true)
        do {
            let htmlString = try NSString(contentsOfFile: htmlPath!, encoding: String.Encoding.utf8.rawValue)
            webView.loadHTMLString(htmlString as String, baseURL: baseUrl)
        } catch {
            // catch error
        }
        
    }

override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
}

override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    if showNav {
    // Hide the navigation bar on the this view controller
    self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    if showNav {
    // Show the navigation bar on other view controllers
    self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}


func setupSpinner(){
    spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height:40))
    spinner.color = UIColor.darkGray
    self.spinner.center = CGPoint(x:UIScreen.main.bounds.size.width / 2, y:UIScreen.main.bounds.size.height / 2)
    self.view.addSubview(spinner)
    spinner.hidesWhenStopped = true
}


// MARK:- WebView Delegates

func webViewDidStartLoad(_ webView: UIWebView) {
    NSLog("Webview started Loading")
    DispatchQueue.main.async {
        self.spinner.startAnimating()
    }
}
func webViewDidFinishLoad(_ webView: UIWebView) {
    print("\nWebview did finish load", terminator: "")
    DispatchQueue.main.async {
        self.spinner.stopAnimating()
    }
}

func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
    print("WebView Failed")
    DispatchQueue.main.async {
        self.spinner.stopAnimating()
    }
}
func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
    
    print("Navigation Url \(request.url!)")
    return true
}


@objc func backAction(sender: UIBarButtonItem) {
    // custom actions here
    if(webView.canGoBack) {
        webView.goBack()
    }
    else {
        self.navigationController?.popViewController(animated: true)
    }
}
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let alertController = UIAlertController(title: "FayeBSG", message: "Please fill all mandatory fields.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            print("OK")
            completionHandler()
        }
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
}

extension ContactUsViewController : WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        GenericMethods.hideLoading()

        print("didFinish navigation:");
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
            let path = Bundle.main.path(forResource:"app", ofType:"css", inDirectory: "Web_Assets/css")
//            guard let path = Bundle.main.path(forResource: "app", ofType: "css") else { return }
            let cssString = try! String(contentsOfFile: path!).trimmingCharacters(in: .whitespacesAndNewlines)
            let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
            webView.evaluateJavaScript(jsString, completionHandler: nil)

        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping ((WKNavigationActionPolicy) -> Void)) {
        
        print("++++++",navigationAction.request.url?.absoluteString)
        
        if (navigationAction.request.url?.absoluteString.contains(find: "forms/userSubmit.jsp"))! {
            self.setupSpinner()
            self.spinner.startAnimating()
            decisionHandler(.allow)
            
        }
        else if (navigationAction.request.url?.absoluteString.contains(find: "fayebsg.com/thanks"))! {
            self.spinner.stopAnimating()
            
            if !self.showNav {
                refreshUrl()
//                NotificationCenter.default.post(name: Notification.Name("ChangeIndex"), object: ["index": 0])
            }
            else {
            if(webView.canGoBack) {
                webView.goBack()
            }
            else {
                self.navigationController?.popViewController(animated: true)
            }
            }
            
            decisionHandler(.cancel)

        }
        else if navigationAction.navigationType == WKNavigationType.linkActivated {
            print("here link Activated!!!")
            
            let url = navigationAction.request.url
            
            let urlStr = url!.absoluteString
            
//            if urlStr.contains(find: "index_contactUs_Blog.html") {
//                self.spinner.startAnimating()
//                webView.load(navigationAction.request)
//            }
//            else {
                UIApplication.shared.open(URL(string : urlStr)!, options: [:], completionHandler: { (status) in
                    
                })
//            }
            
            decisionHandler(WKNavigationActionPolicy.cancel)
        }
        else{
            decisionHandler(WKNavigationActionPolicy.allow)
        }
//        else{
//            decisionHandler(.allow)
//            return
//
//        }
        
        //        if (custom condition){
//            decisionHandler(XX) //  if... return as well
//        } else {
//            decisionHandler(XX)
//        }
    }

}

extension ContactUsViewController : WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        // Callback from javascript: window.webkit.messageHandlers.MyObserver.postMessage(message)
        let text = message.body as! String;
        let alertController = UIAlertController(title: "Javascript said:", message: text, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            print("OK")
        }
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
}


