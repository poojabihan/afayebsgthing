//
//  BlogHeaderCell.swift
//  AFayeBSGThing
//
//  Created by STS-197 on 5/1/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit

class BlogHeaderCell: UICollectionViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var lineLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nameLbl.sizeToFit()
    }

}
