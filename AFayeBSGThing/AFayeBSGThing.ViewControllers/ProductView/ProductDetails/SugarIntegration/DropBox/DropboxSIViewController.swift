//
//  DropboxSIViewController.swift
//  AFayeBSGThing
//
//  Created by Ganesh on 5/7/18.
//  Copyright © 2018 Ganesh. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class DropboxSIViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var youtubeView: YTPlayerView!
    override func viewDidLoad() {
        super.viewDidLoad()
self.navigationItem.title = "Drop Box"
        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
        youtubeView.load(withVideoId: "oL9cil4XMyY")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: ScreenUtils.CurrentDeviceWidth, height: contactView.frame.origin.y + contactView.frame.size.height + 10)
    }

    @IBAction func contactusBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Contact")
    }

    @IBAction func sugarCRMDropboxInt_BuyNowBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "SugarCRMIntegration")
    }
    
    @IBAction func supportBtnAction(_ sender: Any) {
        pushToQuickBooksDetails(type: "Support")
    }
    @IBAction func phoneNumberBtnAction(_ sender: Any) {
        guard let number = URL(string: "tel://" + "8182804820") else { return }
        UIApplication.shared.open(number)
    }
    
    func pushToQuickBooksDetails(type : String) {
        let dropBoxSIWebViewVC = self.storyboard?.instantiateViewController(withIdentifier: "DropBoxSIWebViewVC") as! DropBoxSIWebViewVC
        dropBoxSIWebViewVC.typeOfStr = type
        self.navigationController?.pushViewController(dropBoxSIWebViewVC, animated: true)
    }

}
